# Vanish's Vault FoundryVTT Setup for D&D 5e

I shamelessly ripped this idea from [NateHawk85](https://github.com/NateHawk85/Nathans-DnD5e-Foundry-Modules) (who in turn was inspired by [Bryan's Preferred Modules for Foundry VTT](https://github.com/bryancasler/Bryans-Preferred-Modules-for-FoundryVTT)). As Nathan notes in his repo:

> _I would highly recommend starting out with the core Foundry application and only adding modules if you **absolutely** know what they do and what problems they solve. Additionally, unless you're willing to sit down and debug for a while, I'd recommend sticking to module versions that you've worked with and have tested as opposed to updating on a whim._

This is a list of modules, settings, and resources that I personally use for my FoundryVTT D&D 5th Edition games. There are also copies of some my environment settings within this repository if you want to try the settings that I personally use. Look in the `Configuration Files` directory for files you can import into some modules. If you use the module list to import all modules I use, please note they are all set to active. You will need to deactivate those that aren't applicable to your situation.

I will forewarn you, I have a LOT of modules installed because I believe they enhance the experience or provide quality of life improvements, all of which my players enjoy. Install what intrigues you. I should also note that I host my campaigns on The Forge, so some directories listed are in my Assets Library. Finally, this is an opinionated list of modules and settings. YMMV.

## Foundry VTT v9.269 Settings

In the tables below you will find a list of settings for the FoundryVTT app itself. I initially provided default vs my changes, however the deafults keep chaning or being added to making it more effort to maintain the list. I now only provide the settings as I have them.

### Core Settings

| Setting                         |                                My Settings                                |
| ------------------------------- | :-----------------------------------------------------------------------: |
| **Configure Permissions**       |                                                                           |
| Allow Broadcasting Audio        |      &#9745;&#65039; &#9745;&#65039; &#9745;&#65039; &#9745;&#65039;      |
| Allow Broadcasting Video        |      &#9745;&#65039; &#9745;&#65039; &#9745;&#65039; &#9745;&#65039;      |
| Configure Token Settings        |      &#9745;&#65039; &#9745;&#65039; &#9745;&#65039; &#9745;&#65039;      |
| Create Journal Entries          |            &#128306; &#128306; &#9745;&#65039; &#9745;&#65039;            |
| Create Map Notes                |            &#128306; &#128306; &#9745;&#65039; &#9745;&#65039;            |
| Create Measured Template        |      &#9745;&#65039; &#9745;&#65039; &#9745;&#65039; &#9745;&#65039;      |
| Create New Actors               |            &#128306; &#128306; &#9745;&#65039; &#9745;&#65039;            |
| Create New Items                |            &#128306; &#128306; &#9745;&#65039; &#9745;&#65039;            |
| Create New Tokens               |            &#128306; &#128306; &#9745;&#65039; &#9745;&#65039;            |
| Display Mouse Cursor            |                  &#128306; &#128306; &#128306; &#128306;                  |
| Display Ruler Measurement       |            &#9745;&#65039; &#9745;&#65039; &#128306; &#128306;            |
| Modify Configuration Settings   |               &#128306; &#128306; &#128306; &#9745;&#65039;               |
| Open and Close Doors            |      &#9745;&#65039; &#9745;&#65039; &#9745;&#65039; &#9745;&#65039;      |
| Upload New Files                |            &#128306; &#128306; &#9745;&#65039; &#9745;&#65039;            |
| Use Drawing Tools               |            &#128306; &#128306; &#9745;&#65039; &#9745;&#65039;            |
| Use File Browser                |      &#9745;&#65039; &#9745;&#65039; &#9745;&#65039; &#9745;&#65039;      |
| Use Script Macros               |      &#9745;&#65039; &#9745;&#65039; &#9745;&#65039; &#9745;&#65039;      |
| Whisper Private Messages        |      &#9745;&#65039; &#9745;&#65039; &#9745;&#65039; &#9745;&#65039;      |
| **Audio/Video Configuration**   |                                                                           |
| _General_                       |                                                                           |
| Audio/Video Conferencing Mode   |                            Audio/Video Enabled                            |
| Voice Broadcasting Mode         |                              Always Enabled                               |
| Push-to-Talk / Push-to-Mute     |                                     `                                     |
| _Devices_                       |                                                                           |
| Video Capture Device            |                                  Default                                  |
| Audio Capture Device            |                                  Default                                  |
| Audio Output Device             |                                  Default                                  |
| _Server_                        |                                                                           |
| LiveKit Server                  |                  The Forge (Requires World Builder Tier)                  |
| LiveKit Server Details          |                            No Setup Necessary                             |
| **Default Token Configuration** |                                                                           |
| _Identity_                      |                                                                           |
| Display Name                    |                             Hovered by Anyone                             |
| Token Disposition               |                                  Neutral                                  |
| Show on Tokenbar                |                                  Default                                  |
| Lock Rotation                   |                                 &#128306;                                 |
| Token Dialog Journal Entry      |                                                                           |
| Tags (separarted by commas)     |                                                                           |
| _Vision_                        |                                                                           |
| Has Vision                      |                              &#9745;&#65039;                              |
| Vision Rules                    |                      Default (Dungeon & Dragons 5e)                       |
| Monochrome Vision Color         |                             Default (#ffffff)                             |
| Vision Radius (Grid Units)      |                             Dim: 0 Bright: 0                              |
| Vision Angle (Degrees)          |                                    360                                    |
| Sight Limit                     |                                 Unlimited                                 |
| _Light_                         |                                                                           |
| - _Basic Configuration_         |                                                                           |
| Light Radius (Grid Units)       |                             Dim: 0 Bright: 0                              |
| Emission Angle (Degrees)        |                                    360                                    |
| Light Color                     |                                  #000000                                  |
| Color Intensity                 |                                    0.5                                    |
| - _Light Animation_             |                                                                           |
| Light Animation Type            |                                   None                                    |
| Animation Speed                 |                                     5                                     |
| Reverse Direction               |                                 &#128306;                                 |
| Animation Intensity             |                                     5                                     |
| - _Advanced Options_            |                                                                           |
| Coloration Technique            |                            Adaptive Luminance                             |
| Luminosity                      |                                    0.5                                    |
| Gradual Illumination            |                              &#9745;&#65039;                              |
| Background Saturation           |                                     0                                     |
| Background Contrast             |                                     0                                     |
| Background Shadows              |                                     0                                     |
| Priority                        |                                     0                                     |
| Sight Limit                     |                            &#128306; Infinity                             |
| _Resources_                     |                                                                           |
| Display Bars                    |                             Hovered by Anyone                             |
| Bar 1 Attribute                 |                               attributes.hp                               |
| Bar 2 Attribute                 |                                   None                                    |
| **Terrain**                     |                                                                           |
| _Terrain_                       | _Any_ \| _Walking_ \| _Swimming_ \| _Flying_ \| _Burrowing_ \| _Climbing_ |
| All                             |        &#128306; &#128306; &#128306; &#128306; &#128306; &#128306;        |
| Arctic                          |        &#128306; &#128306; &#128306; &#128306; &#128306; &#128306;        |
| Coast                           |        &#128306; &#128306; &#128306; &#128306; &#128306; &#128306;        |
| Desert                          |        &#128306; &#128306; &#128306; &#128306; &#128306; &#128306;        |
| Forest                          |        &#128306; &#128306; &#128306; &#128306; &#128306; &#128306;        |
| Grassland                       |        &#128306; &#128306; &#128306; &#128306; &#128306; &#128306;        |
| Jungle                          |        &#128306; &#128306; &#128306; &#128306; &#128306; &#128306;        |
| Mountain                        |  &#128306; &#128306; &#128306; &#128306; &#9745;&#65039; &#9745;&#65039;  |
| Swamp                           |        &#128306; &#128306; &#128306; &#128306; &#128306; &#128306;        |
| Underdark                       |        &#128306; &#128306; &#128306; &#128306; &#128306; &#128306;        |
| Urban                           |        &#128306; &#128306; &#128306; &#128306; &#128306; &#128306;        |
| Water                           |     &#128306; &#128306; &#9745;&#65039; &#128306; &#128306; &#128306;     |
| **Health Estimate**             |                                                                           |
| Don't Mark as Dead              |                                 &#128306;                                 |
| Hide Estimates                  |                                 &#128306;                                 |
| Hide Names                      |                                 &#128306;                                 |
| **General Settings**            |                                                                           |
| Disable Game Canvas             |                                 &#128306;                                 |
| Language Preference             |                                  English                                  |
| Enable Chat Bubbles             |                              &#9745;&#65039;                              |
| Pan to Token Speaker            |                                 &#128306;                                 |
| Scrolling Status Text           |                              &#9745;&#65039;                              |
| Left-Click to Release Objects   |                              &#9745;&#65039;                              |
| Perfrmance Mode                 |                                  Maximum                                  |
| Maximum Framerate               |                                    60                                     |
| Show FPS Meter                  |                              &#9745;&#65039;                              |
| Font Size                       |                                     5                                     |
| Token Drag Vision               |                                 &#128306;                                 |

### System Settings: DnD5e - Fifth Edition v1.6.2

| Setting                            |                 My Settings                 |                                            Comments                                            |
| ---------------------------------- | :-----------------------------------------: | :--------------------------------------------------------------------------------------------: |
| Rest Variant                       | Player's Handbook (LR: 8 hours, SR: 1 hour) |                                                                                                |
| Diagonal Movement Rule             |          PHB: Equidistant (5/5/5)           |                                                                                                |
| Proficiency Variant                |       PHB: Bonus (+2, +3, +4, +5, +6)       |                                                                                                |
| Honor Ability Score                |                  &#128306;                  |                                                                                                |
| Sanity Ability Score               |                  &#128306;                  |                                                                                                |
| Initiative Dexterity Tiebreaker    |               &#9745;&#65039;               |                                                                                                |
| Apply Currency Weight              |               &#9745;&#65039;               |                                                                                                |
| Disable Experience Tracking        |                  &#128306;                  |                Set this to &#9745;&#65039; if you are using Milestone leveling.                |
| Disable Level Up Automation        |                  &#128306;                  |                                                                                                |
| Collapse Item Cards In Chat        |                  &#128306;                  |                                                                                                |
| Allow Polymorphing                 |                  &#128306;                  | In order for your players to polymorph, they need to have the permission to create new tokens. |
| Use Metric Weight Units            |                  &#128306;                  |                                                                                                |
| Critical Damage Multiply Modifiers |                  &#128306;                  |                                                                                                |
| Critical Damage Maximize Dice      |               &#9745;&#65039;               |                                                                                                |

## The Great FoundryVTT Module List

- :part_alternation_mark: = Must Have Module
- :parking: = Requires a Patreon subscription for added value

The descriptions provided below are from the module authors. If I have more to say about a module it will be listed as a **NOTE**. I also note dependencies and available settings for each module.

### [5e Statblock Importer](https://www.foundryvtt-hub.com/package/5e-statblock-importer/) v1.4.5

Easily import 5e Monster and NPC statblocks into your game as long as they're formatted using the standard WotC layout. It will create a new actor with an NPC character sheet using those stats.

### [About Time](https://www.foundryvtt-hub.com/package/about-time/) v1.0.6

A module to support a game time clock in Foundry VTT. Arbitrary calendars are supported Gregorian and Greyhawk supplied but it is easy to add your own.

DateTime Arithmentic is supported allowing calculations like DateTime.Now().add({days:1}) (increments can be negative, but must be integers)

DateTime represents a date and time comprising ({years, months, days, hours, minutes, seconds}) and repsents an actual date and time. DTMod is a class for representing time intervals, game.Gametime.DTMod.create({years, months, days, hours. minutes, seconds}) only represents an interval to be applied to a DateTime. Typically you would do things like DateTime.now().add({days: 5})

The gametime clock can bet set/advanced as you wish. The clock can also be set to run in real time at a multiple/fraction of real world time. The game clock is snchronised betwee all clients and time updates are broadcast to each client. You can register for them via Hooks.on("clockUpdate").

The real use for the clock is having events fire at predefined times or intervals. You can call arbitrary code or macros based off the game time clock. Allowing things like game.Gametime.doEvery({hours:24}, "Eat Food")

Dependencies: [SimpleCalendar](https://www.foundryvtt-hub.com/package/foundryvtt-simple-calendar/)

| Setting      | My Setting | Comments |
| ------------ | :--------: | :------: |
| Debug Output | &#128306;  |          |

### [Active-Auras](https://www.foundryvtt-hub.com/package/activeauras/) v0.4.13

Active Auras will propagate Active Effects that are labeled as auras onto nearby tokens. The distance and targeting of the aura are both configurable. Any @ fields from DAE will be correctly parsed before being applied to the effected token. Macros from DAE will fire once when applied and once when removed.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/lib-wrapper/), [Socketlib](https://www.foundryvtt-hub.com/package/socketlib/)

| Setting                           |   My Setting    | Comments |
| --------------------------------- | :-------------: | :------: |
| Measurement System                | &#9745;&#65039; |          |
| Walls Block Auras                 |    &#128306;    |          |
| Height Measurement System         | &#9745;&#65039; |          |
| Remove Auras on Death             | &#9745;&#65039; |          |
| Auras in Combat                   | &#9745;&#65039; |          |
| Disable Scrolling Check for Auras | &#9745;&#65039; |          |
| Debug                             |    &#128306;    |          |

### [Active Token Effects](https://www.foundryvtt-hub.com/package/) v0.4.01

Active Token Effects works as active effects for token data. This means any token data can be controlled through an Active Effect: size, lighting, vision, image etc.

| Setting                    |   My Setting    | Comments |
| -------------------------- | :-------------: | :------: |
| Size Adjustment with Flags | &#9745;&#65039; |          |

### [Actor Link Indicator](https://www.foundryvtt-hub.com/package/) v0.0.3

Stop forgetting to link/unlink tokens from actors! Adds an easy to observe, toggle indicator in sheet titles to quickly switch and identify whether actors produce linked tokens or unlinked prototype tokens, or whether concrete Tokens are currently linked to their Actor or not.

### [Advanced Macros](https://www.foundryvtt-hub.com/package/advanced-macros/) v1.14.2

Adds a `Run Macro` button to quickly test macros. Provides Handlebars templating of chat macros prefixed with `/` and recursive chat commands.

**NOTE:** I only have this as a dependency for [Dynamic Active Effects SRD](https://www.foundryvtt-hub.com/package/dynamic-effects-srd/)

| Setting               |   My Setting    | Comments |
| --------------------- | :-------------: | :------: |
| Highlight Macro Codes | &#9745;&#65039; |          |

### [Advanced Spell Effects](https://www.foundryvtt-hub.com/package/advancedspelleffects/) v0.9.5.6

Cool JB2A animated effects for some spells. Eligible spells will display an "ASE" tab at the top of the item - this can be used to convert a spell to an ASE spell.

Dependencies: [Sequencer](https://www.foundryvtt-hub.com/package/sequencer/), [SocketLib](https://www.foundryvtt-hub.com/package/socketlib/), [Tagger](https://www.foundryvtt-hub.com/package/tagger/), [Warpgate](https://www.foundryvtt-hub.com/package/warpgate/)

| Setting                             |   My Setting    |                   Comments                    |
| ----------------------------------- | :-------------: | :-------------------------------------------: |
| Enable ASE Grid Highlight Override  | &#9745;&#65039; |                                               |
| Enable ASE Template Border Override |    &#128306;    | Turning this on conflicts with Token Magic FX |

### [Ambient Doors](https://www.foundryvtt-hub.com/package/ambientdoors/) v2.2.6

A controller for playing ambient sounds effects when opening and closing doors.

| Setting                         |                       My Setting                       | Comments |
| ------------------------------- | :----------------------------------------------------: | :------: |
| Silent Door Permission Level    |                       Assistant                        |          |
| Door Close                      | modules/ambientdoors/defaultSounds/DoorCloseSound.wav  |          |
| Close Door Volume Level         |                          0.8                           |          |
| Door Open                       |  modules/ambientdoors/defaultSounds/DoorOpenSound.wav  |          |
| Door Open Volume Level          |                          0.8                           |          |
| Door Lock                       |  modules/ambientdoors/defaultSounds/DoorLockSound.wav  |          |
| Door Lock Volume Level          |                          0.8                           |          |
| Door Unlock                     | modules/ambientdoors/defaultSounds/DoorUnlockSound.wav |          |
| Door Unlock Volume Level        |                          0.8                           |          |
| Locked Door Jingle              |                    sounds/lock.wav                     |          |
| Locked Door Jingle Volume Level |                          0.8                           |          |

### [Ammo Tracker](https://www.foundryvtt-hub.com/package/ammo-tracker-fvtt/) v0.2.3

Tired of keeping track of how many arrows, bolts, etc. you used during the combat? Ammo Tracker does that for you and lets you automatically recover the ammunition used.
**NOTE:** Ammo is tracked only during an encounter.

Dependencies: [Socketlib](https://www.foundryvtt-hub.com/package/socketlib/)

### [Arius Planeswalkers's Stlyish Journal for Monk's Enhanced Journal](https://www.foundryvtt-hub.com/package/apsj4mej/) v2.6.0

Need to draw attention to important information in your journals? Panels have you covered, from informative to notes to warnings and bonuses. Like the D&D style text? Fake it till you make it with D&D Text Styles you can apply to Headings and body text. No more boring encounter pages. Liven it up with the Encounter Block, complete with Adversary listing! Add some panache to your Magic Item listings and pull out the important stats and item links to a quickly accessible aside. Have a description to read aloud and need a way to differentiate it on the page? Read Aloud block to the rescue! Stop looking at those boring old pale pages. Add some vibrance and style to your enhanced journal entries todav!

Dependencies: [Monk's Enhanced Journal](https://www.foundryvtt-hub.com/package/monks-enhanced-journal/)

| Setting          |   My Setting    | Comments |
| ---------------- | :-------------: | :------: |
| Enable Parchment | &#9745;&#65039; |          |

### [Autocomplete Inline Properties](https://www.foundryvtt-hub.com/package/autocomplete-inline-properties/) v2.7.6

Add inline property autocompletion for fields that can use inline properties or otherwise reference document data.

| Setting    |   My Setting    | Comments |
| ---------- | :-------------: | :------: |
| Debug Mode | &#9745;&#65039; |          |

### [Auto-Rotate](https://www.foundryvtt-hub.com/package/autorotate/) v1.2.8

Allows tokens to automatically rotate based on triggers such as movement, targeting, etc.

| Setting               | My Setting | Comments |
| --------------------- | :--------: | :------: |
| Default Rotation Mode | Automatic  |          |

### [Automated Animations](https://www.foundryvtt-hub.com/package/autoanimations/) v3.2.1

This module leverages the JB2A Module (Free or Patreon) to automatically play animations when items are activated. It will automatically detect the name of the item and play a given animation from the JB2A module that matches. The intent is for this to be used with "instant" effects, such as Weapon Attacks, Attack Spells, Instant AOE effects, and Healing spells. The automation is limited to the effects provided by the JB2A module.

Dependencies: [JB2A](https://www.foundryvtt-hub.com/package/jb2a_dnd5e/) or [JB2A - Patreon Complete Collection](https://www.foundryvtt-hub.com/package/jb2a_patreon/), [Sequencer](https://www.foundryvtt-hub.com/package/sequencer/)
Optional Dependencies: [Socketlib](https://www.foundryvtt-hub.com/package/socketlib/) (for placing Tile effects)

| Setting                              |                                                                     My Setting                                                                      |                          Comments                          |
| ------------------------------------ | :-------------------------------------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------: |
| **Configure Automatic Recognition**  |                                                            I leave these at the defaults                                                            | Import file available in the Configurtion Files directory. |
| **General Settings**                 |                                                                                                                                                     |                                                            |
| Toggle Animations                    |                                                                         ON                                                                          |                                                            |
| Disable Automatic Recognition        |                                                                      &#128306;                                                                      |                                                            |
| Global Delay                         |                                                                         100                                                                         |                                                            |
| Animation Preview                    |                                                             Manually Play Video Preview                                                             |                                                            |
| JB2A Asset Location                  |                                                                                                                                                     |                                                            |
| Hide the Automation Tab From Players |                                                                      &#128306;                                                                      |                                                            |
| Decouple Sounds                      |                                                                      &#128306;                                                                      |                                                            |
| Disable Melee to Range Switch        |                                                                      &#128306;                                                                      |                                                            |
| Disable Persistant Effect Tip        |                                                                      &#128306;                                                                      |                                                            |
| Disable Active Effect Animations     |                                                                      &#128306;                                                                      |                                                            |
| Only Play Animations on Hits         |                                                                      &#128306;                                                                      |                                                            |
| Play Animations on Hits and Misses   |                                                                   &#9745;&#65039;                                                                   |                                                            |
| Only play animations on Damage Rolls |                                                                      &#128306;                                                                      |                                                            |
| Critical Hits                        |                                                                   &#9745;&#65039;                                                                   |                                                            |
| Critical Hit Animation               | "https://assets.forge-vtt.com/bazaar/modules/jb2a_patreon-2d960ceba80cc778/assets/Library/1st_Level/Bless/Bless_01_Regular_Blue_Intro_400x400.webm" |                                                            |
| Critical Misses                      |                                                                   &#9745;&#65039;                                                                   |                                                            |
| Critical Miss Animation              |      "https://assets.forge-vtt.com/bazaar/modules/jb2a_patreon-2d960ceba80cc778/assets/Library/1st_Level/Grease/Grease_Dark_Grey_600x600.webm"      |                                                            |
| Enable Debugging                     |                                                                      &#128306;                                                                      |                                                            |

### [Automated Evocations - Companion Manager](https://www.foundryvtt-hub.com/package/automated-evocations/) v1.6

A user interface to manage companions with summoning animations and automated summoning for spells.

Dependencies: [Sequencer](https://www.foundryvtt-hub.com/package/sequencer/), [Warp Gate](https://www.foundryvtt-hub.com/package/warpgate/)

| Setting                   |   My Setting    | Comments |
| ------------------------- | :-------------: | :------: |
| Auto-Close                | &#9745;&#65039; |          |
| Enable Automations        | &#9745;&#65039; |          |
| Store Companions on Actor |    &#128306;    |          |
| Hide Companion Button     |    &#128306;    |          |
| Only Owned Actors         | &#9745;&#65039; |          |

### [Automatic Journal Icon Numbers](https://www.foundryvtt-hub.com/package/journal-icon-numbers/) v1.7.2

This module will automatically apply numbered icons (map pins) to journal entries that start with a number. It supports the following numbering formats automatically, with both upper and lower cases.

- [0-9999]
- [A-Z][0-999]
- [0-999][a-z]

Dependencies: [Lib - Color Settings](https://www.foundryvtt-hub.com/package/colorsettings/)

| Setting                            |            My Setting             | Comments |
| ---------------------------------- | :-------------------------------: | :------: |
| **Default Icon Style**             |                                   |          |
| Text for sample icon below         |                42a                |          |
| Icon scale                         |               0.75                |          |
| Mouse-over font size               |                48                 |          |
| Icon Style                         |              Circle               |          |
| Font Family                        |             Open Sans             |          |
| Bold                               |          &#9745;&#65039;          |          |
| Italics                            |             &#128306;             |          |
| Icon Font Size                     |                200                |          |
| Border Width                       |                10                 |          |
| Foreground Colors                  |             #000000ff             |          |
| Background Colors                  |             #d0d0d080             |          |
| **Name Matching Algorithm**        |                                   |          |
| Text for sample icon below         |                42a                |          |
| A letter + numbers (A24)           |          &#9745;&#65039;          |          |
| Numbers + a letter (23G)           |          &#9745;&#65039;          |          |
| Just numbers (123)                 |          &#9745;&#65039;          |          |
| A letter followed by a space (Q )  |             &#128306;             |          |
| A letter followed by a period (Z.) |             &#128306;             |          |
| Custom RegEx                       |                                   |          |
| **General Settings**               |                                   |          |
| Icons from folders                 |          &#9745;&#65039;          |          |
| Quick Add Mode                     |          &#9745;&#65039;          |          |
| Upload path                        | custom/icons/journal-icon-numbers |          |
| Rebuild all icons                  |            No rebuild             |          |

### [Babele](https://www.foundryvtt-hub.com/package/babele/) v2.2.5

Babele is a module for runtime translation of Compendium packs.

Babele obtains translation from configuration files in json format and applies them on the original compendium, overwriting the mapped properties of the entity in memory without altering the original content, in this way it is not necessary to keep a compendium copy with the sole purpose of translating their contents.

It is also possible to provide a mapping of the translated fields, so that you can add different properties based on the pack you want to translate.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/lib-wrapper/)

| Setting                     |     My Setting      | Comments |
| --------------------------- | :-----------------: | :------: |
| Translations File Directory | custom/translations |          |
| Enable Translation Export   |   &#9745;&#65039;   |          |
| Shows Original Name         |   &#9745;&#65039;   |          |
| Show Translate Option       |   &#9745;&#65039;   |          |

### [Backgroundless Pins](https://www.foundryvtt-hub.com/package/backgroundless-pins/) v1.2.03

Removes the background box from map notes.

### [Bean Dice](https://www.foundryvtt-hub.com/package/bean-dice/) v0.1.1

This module adds a bean texture for Dice-So-Nice

Dependencies: [Dice So Nice](https://www.foundryvtt-hub.com/package/dice-so-nice/)

### [Better Roll Tables](https://www.foundryvtt-hub.com/package/better-rolltables/) v1.8.95

Enhance your RollTables with Drag and Drop, Generate Loot, Encounters and and build your NPC Generator! Some Features:

- Roll on multiple tables with roll formulas
- Auto create a loot actor to store generated loot
- Automatically select a random spells when a scroll item is selected

| Setting                                       |   My Setting    |                  Comments                   |
| --------------------------------------------- | :-------------: | :-----------------------------------------: |
| **User Interface Integration**                |                 | Doesn't appear to be functioning at present |
| Roll from compendium context menu             | &#9745;&#65039; |                                             |
| Add roll button to tables in journal entries? | &#9745;&#65039; |                                             |
| Use Tags                                      | &#9745;&#65039; |                                             |

### [Better Roofs](https://www.foundryvtt-hub.com/package/betterroofs/) :part_alternation_mark: v1.5.5

A module to improve roof behaviour.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/lib-wrapper/)

| Setting                         |   My Setting    | Comments |
| ------------------------------- | :-------------: | :------: |
| Visibility of tiles through fog |       0.9       |          |
| Occlusion radius                |        1        |          |
| Performance mode                |    &#128306;    |          |
| Advanced mode                   | &#9745;&#65039; |          |
| Weather Blocker integration     | &#9745;&#65039; |          |

### [Better Text Drawings](https://www.foundryvtt-hub.com/package/better-text-drawings/) v1.1.1.0

Module that adds more options to text drawings, allowing multi-line text as well as configurable stroke width, color, and text alignment.

### [Break Time](https://www.foundryvtt-hub.com/package/breaktime/) v1.0.14

A module to track when players have returned from a break.

| Setting             |           My Setting            | Comments |
| ------------------- | :-----------------------------: | :------: |
| Automatically Pause |         &#9745;&#65039;         |          |
| Show Button         |         &#9745;&#65039;         |          |
| Away Message        | I'm stepping away for a second. |          |
| Back Message        |            I'm back.            |          |

### [Bug Reporter](https://www.foundryvtt-hub.com/package/bug-reporter/) v1.3.3

Adds a utility in game to report bugs without having to leave Foundry.

### [Bulk Tasks](https://www.foundryvtt-hub.com/package/bulk-tasks/) v0.3.0

Delete/Move entities en masse.

| Setting                |   My Setting    | Comments |
| ---------------------- | :-------------: | :------: |
| Hide Menu From Players | &#9745;&#65039; |          |

### [Catch Up](https://www.foundryvtt-hub.com/package/catch-up/) v1.0.1

A module that makes player clients catch up to changes to the world that happened while the client was loading.

### [Chat Log Scaler](https://www.foundryvtt-hub.com/package/chatlog-scaler/) v0.0.4

Enables you to change the font sizes in the Chat Log panel using ctrl+mouseWheelUp and ctrl+mouseWheelDown.

Instructions
Step 1: activate this module in your world
Step 2: hover your mouse cursor over the Chat Log panel
Step 3: hold down ctrl and use your mouse wheel to scroll either up or down to increase or decrease the font sizes, respectively

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/lib-wrapper/)

### [Chat Message Accessibility Indicators](https://www.foundryvtt-hub.com/package/chat-indicators/) v1.0.0

Adds a textual tag to chat messages to easily identify whether the message is a whisper, blind roll, or self roll. Additionally, makes it easier to identify the participants in a whisper.

### [Chat Portrait](https://www.foundryvtt-hub.com/package/chat-portrait/) v0.7.4

A module that displays the Actor's portrait images on the chat messages.

| Setting                                                                 |                     My Setting                     | Comments |
| ----------------------------------------------------------------------- | :------------------------------------------------: | :------: |
| Use token image                                                         |                  &#9745;&#65039;                   |          |
| Do not use Token Image with the following types of actors               |                                                    |          |
| Use token Name                                                          |                     &#128306;                      |          |
| Use Avatar Player Image                                                 |                     &#128306;                      |          |
| Shows the name of the Player next to the name of the Actor              |                     &#128306;                      |          |
| Portrait size (px)                                                      |                         36                         |          |
| Portrait size for Item (px)                                             |                         36                         |          |
| Portrait border shape                                                   |                       Circle                       |          |
| Use Player color for portrait border                                    |                  &#9745;&#65039;                   |          |
| Portrait border color                                                   |                      #000000                       |          |
| Portrait border width (px)                                              |                         2                          |          |
| Chamge color of message border                                          |                  &#9745;&#65039;                   |          |
| Change color of message background                                      |                     &#128306;                      |          |
| Display chat flavor text next to portrait                               |                  &#9745;&#65039;                   |          |
| Force name search                                                       |                     &#128306;                      |          |
| Size of the name text on chat (px)                                      |                         0                          |          |
| Adds a textual tag to chat messages                                     |                     &#128306;                      |          |
| Use image replacer feature                                              |                  &#9745;&#65039;                   |          |
| Use image replacer for damage type feature                              |                     &#128306;                      |          |
| Apply chat portrait on Combat Tracker                                   |                     &#128306;                      |          |
| Apply patch for 'PreCreateChatMessage'                                  |                     &#128306;                      |          |
| Display Settings                                                        |                Affect Every Message                |          |
| Display chat message of type 'OTHER'                                    |                  &#9745;&#65039;                   |          |
| Display chat message of type 'OOC'                                      |                  &#9745;&#65039;                   |          |
| Display chat message of type 'IC'                                       |                  &#9745;&#65039;                   |          |
| Display chat message of type 'EMOTE'                                    |                  &#9745;&#65039;                   |          |
| Display chat message of type 'WHISPER'                                  |                  &#9745;&#65039;                   |          |
| Display chat message of type 'ROLL'                                     |                  &#9745;&#65039;                   |          |
| Display chat messages of whisper to other                               |                     &#128306;                      |          |
| Display Unknown                                                         |              Don't Affect Any Messges              |          |
| Placeholder for the label unknown Actor name                            |                   Unknown Actor                    |          |
| Placeholder for the label unknown Item name                             |                    Unknown Item                    |          |
| Placeholder for the label unknown Item icon                             | /modules/chat-portrait/assets/inv-unidentified.png |          |
| Setup additional custom styling on the text chat message                |                                                    |          |
| Disable the chat portrait if the alias of the chat message is from a GM |                     &#128306;                      |          |
| Setup a specific image if the alias of the chat message is from a GM    |                                                    |          |
| Enable Debugging                                                        |                     &#128306;                      |          |

### [Cleaner Sheet Title Bar](https://www.foundryvtt-hub.com/package/cleaner-sheet-title-bar/) v1.3.7

This module remove text from actions in cluttered sheet title bar. Text still available on mouse hover. Work with all
systems and all modules!

### [Colored Folder Contents](https://www.foundryvtt-hub.com/package/colored-folder-contents/) v1.01

A simple module to apply folders' background colors to their contents.

### [Combat Booster](https://www.foundryvtt-hub.com/package/combatbooster/) v2.6

Speed up your combat with Recent actions(dnd5e only), Turn Marker and other smaller tweaks

| Setting                         |                  My Setting                  | Comments |
| ------------------------------- | :------------------------------------------: | :------: |
| Enable turn marker              |               &#9745;&#65039;                |          |
| Turn marker image               | modules/combatbooster/markers/blueRunes.webp |          |
| Turn marker rotation speed      |                      10                      |          |
| Turn marker scale               |                      1                       |          |
| Turn Marker Transparency        |                     0.8                      |          |
| Turn marker above               |                  &#128306;                   |          |
| Enable Comabt HUD (dnd5e only)  |               &#9745;&#65039;                |          |
| Recent Items                    |                      4                       |          |
| Combat HUD columns              |                      4                       |          |
| Mark defeated                   |               &#9745;&#65039;                |          |
| Body Pile                       |                  &#128306;                   |          |
| Pan Camera                      |                  &#128306;                   |          |
| Auto-Select Token on Turn Start |               &#9745;&#65039;                |          |
| Ignore Player Tokens            |                  &#128306;                   |          |
| Auto-Enable HUD                 |                  &#128306;                   |          |
| Your turn sound                 |                                              |          |
| Sound volume                    |                     0.4                      |          |
| Your turn notification          |                  &#128306;                   |          |
| Current HP                      |           data.attributes.hp.value           |          |
| Max HP                          |            data.attributes.hp.max            |          |

### [Combat Carousel](https://www.foundryvtt-hub.com/package/combat-carousel/) v0.2.5

A module to display CRPG-style Combatant cards in carousel-like format.

| Setting                                 |        My Setting        | Comments |
| --------------------------------------- | :----------------------: | :------: |
| **Overlay Config**                      |                          |          |
| Property 0 Name                         |            AC            |          |
| Property 0 Value                        |   attributes.ac.value    |          |
| Property 0 Icon                         |   icons/svg/shield.svg   |          |
| Property 1 Name                         |        Perception        |          |
| Property 1 Value                        |    skills.prc.passive    |          |
| Property 1 Icon                         |    icons/svg/eye.svg     |          |
| Property 2 Name                         |                          |          |
| Property 2 Value                        |                          |          |
| Property 2 Icon                         | /icons/svg/d20-black.svg |          |
| Property 3 Name                         |                          |          |
| Property 3 Value                        |                          |          |
| Property 3 Icon                         | /icons/svg/d20-black.svg |          |
| **General Settings**                    |                          |          |
| Enable Combat Carousel                  |     &#9745;&#65039;      |          |
| Collapse Navigation Bar                 |        &#128306;         |          |
| Open Combat Carousel on Combat Creation |     &#9745;&#65039;      |          |
| Carousel Size                           |          Medium          |          |
| Combatant Image                         |          Token           |          |
| Control Active Combatant's Token        |        &#128306;         |          |
| Pan on Card Click                       |     &#9745;&#65039;      |          |
| Always on Top                           |        &#128306;         |          |
| Show Overlay                            |         On Hover         |          |
| Player Overlay Permissions              |     Observed Actors      |          |
| Show Effects                            |       All Effects        |          |
| Show Initiative                         |          Always          |          |
| Show Initiative Icon                    |          Always          |          |
| Player Initiative Permission            |          Owned           |          |
| Show Resource Bar 1                     |          Always          |          |
| Player Bar 1 Permissions                |     Observed Actors      |          |
| Bar 1 Attribute                         |      attributes.hp       |          |
| Bar 1 Name                              |            HP            |          |

### [Compact Compendium List](https://www.foundryvtt-hub.com/package/compact-compendium-list/) v0.0.6

Better UI for the compendium packs tab. Clearer and more compact.

### [Compendium Folders](https://www.foundryvtt-hub.com/package/compendium-folders/) :part_alternation_mark: v2.4.6

Adds a nested folder structure to organize your compendiums, and to organize contents of a compendium. This allows you to hide compendiums completely, in addition to being able to export folder structures into a compendium for later use.

| Setting                       |   My Setting    | Comments |
| ----------------------------- | :-------------: | :------: |
| Default Merge by Name         | &#9745;&#65039; |          |
| Default Keep ID               | &#9745;&#65039; |          |
| Auto Create Folders on Import | &#9745;&#65039; |          |

### [Conditional Visibility](https://www.foundryvtt-hub.com/package/conditional-visibility/) v.0.6.14

Hide tokens from some players, but not others, based on the senses the players have.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/libwrap/), [Socketlib](https://www.foundryvtt-hub.com/package/socketlib/)

| Setting                                                         |   My Setting    | Comments |
| --------------------------------------------------------------- | :-------------: | :------: |
| Enable Sight Check for GM                                       | &#9745;&#65039; |          |
| CV Hud Element                                                  |    &#128306;    |          |
| HUD Column                                                      |      Left       |          |
| HUD Top/Bottom                                                  |       Top       |          |
| Disable for Non-Hostile NPC                                     |    &#128306;    |          |
| Disable Automatic Import on Module "DFred's Convenient Effects" |    &#128306;    |          |
| Auto Apply Effect for Preconfigured Skills                      | &#9745;&#65039; |          |
| Skip Dialog Confirmation for "Auto Apply Preconfigured Skills"  |    &#128306;    |          |
| [EXPERIMENTAL] Enable Draw CV Handler                           |    &#128306;    |          |
| Enable Refresh Sight CV Handler                                 |    &#128306;    |          |
| Enable Debugging                                                |    &#128306;    |          |

### [Crash's Tracking & Training (5e)](https://www.foundryvtt-hub.com/package/5e-training/) v0.6.9

Adds a tab to all actor sheets (character and NPC) that lets you add and keep track of just about anything you can keep track of with a number and a progress bar.

| Setting                          |   My Setting    | Comments |
| -------------------------------- | :-------------: | :------: |
| Show Import Button               |    &#128306;    |          |
| Enable GM Only Mode              |    &#128306;    |          |
| Show Tracking Tab (PCs)          | &#9745;&#65039; |          |
| Show Tracking Tab (NPCs)         | &#9745;&#65039; |          |
| Tab Name                         |    Downtime     |          |
| Character Sheet Width Adjustment |       50        |          |
| Default Completion Target        |       300       |          |
| Announce Completion For          |    PC's Only    |          |

### [D&D Beyond Gamelog](https://www.foundryvtt-hub.com/package/ddb-game-log/) :parking: v1.6.2

Display your D&D Beyond rolls in Foundry VTT with ease!

- No browser extensions or iframes needed and literally you can roll for [EVERY SINGLE THING].
- D&D Beyond (on desktop) and the D&D Beyond Player app (iOS/Android including phones and tablets) are also supported.
- Patreon subscription optional, but worth the PRO tier

| Setting                               |           My Setting           |                                                   Comments                                                    |
| ------------------------------------- | :----------------------------: | :-----------------------------------------------------------------------------------------------------------: |
| **Core Settings**                     |                                |                                                                                                               |
| **Basic**                             |                                |                      I recommend using the DDB Data Grabber extension to fill this out.                       |
| **Mapping**                           |                                |                                                                                                               |
| Display Actors Without an Owner       |           &#128306;            |                                                                                                               |
| Display Non-Player Character Actors   |           &#128306;            |                                                                                                               |
| Try to Autofill the Mapping           |        &#9745;&#65039;         |                                    Requires installing D&D Beyond Importer                                    |
| **Integrations**                      |                                |                                                                                                               |
| _Roll Settings_                       |                                |                                                                                                               |
| Display Pending Rolls                 |        &#9745;&#65039;         |                                                                                                               |
| Auto-hide Pending Rolls               |        &#9745;&#65039;         |                                                                                                               |
| Exclude Unmapped Rolls                |           &#128306;            |                                                                                                               |
| Character Name Click Behavior         | Open Character Sheet (Foundry) |                                                                                                               |
| _Basic Automation_                    |                                |                                                                                                               |
| Enable Initiative Tracking            |        &#9745;&#65039;         |                                                                                                               |
| Confirm Value Override                |        &#9745;&#65039;         |                                                                                                               |
| _Custom Themes_                       |                                |                                               **Pro** Settings                                                |
| Chat Messages Theme                   |           Dark Theme           |                                                                                                               |
| Border Color Override                 |        &#9745;&#65039;         |                                                                                                               |
| _Roll Result Breakdown_               |                                |                                               **Pro** Settings                                                |
| Enable Roll Result Breakdown          |        &#9745;&#65039;         |                                                                                                               |
| _Avatar Settings_                     |                                |                                               **Pro** Settings                                                |
| Display Avatar Images                 |        &#9745;&#65039;         |                                                                                                               |
| Avatar Image Source                   |   D&D Beyond Character Sheet   |                                                                                                               |
| Avatar Click Behavior (As a GM)       |           Do Nothing           |                                        Open Character Sheet (Foundry)                                         |
| _D&D Beyond Player App Integration_   |                                |                                               **Pro** Settings                                                |
| Enable Player App Integration         |        &#9745;&#65039;         |                                                                                                               |
| _Character Updater (Beta)_            |                                |                                               **Pro** Settings                                                |
| Enable Character Updater (Global)     |        &#9745;&#65039;         |                                                                                                               |
| Enable Hit Point Updates              |        &#9745;&#65039;         |                                                                                                               |
| Enable Exhaustion Level Updates       |        &#9745;&#65039;         |                                                                                                               |
| Enable Inspiration Point Update       |        &#9745;&#65039;         |                                                                                                               |
| Enable Death Save Updates             |        &#9745;&#65039;         |                                                                                                               |
| Enable XP Updates                     |        &#9745;&#65039;         |                                                                                                               |
| Enable Level Updates                  |        &#9745;&#65039;         |                                                                                                               |
| _Defenses Updater (Beta)_             |                                |                                               **Pro** Settings                                                |
| Enable Damage Resistances Updates     |        &#9745;&#65039;         |                                                                                                               |
| Enable Damage Immunities Updates      |        &#9745;&#65039;         |                                                                                                               |
| Enable Damage Vulnerabilities Updates |        &#9745;&#65039;         |                                                                                                               |
| Enable Condition Immunities Updates   |        &#9745;&#65039;         |                                                                                                               |
| _Condition Updater (Beta)_            |                                | **Pro** Settings - Requires Combat Utility Belt module which I have replaced with DFred's Convenient Effects. |
| Enable Conditon Updates               |        &#9745;&#65039;         |                                                                                                               |
| Respect Custom Conditions             |        &#9745;&#65039;         |                                                                                                               |
| _Dice So Nice! Integration_           |                                |                               **Pro** Settings - Requires Dice So Nice! module                                |
| Enable Dice So Nice! Integration      |        &#9745;&#65039;         |                                                                                                               |
| Message Timing                        |   Before the Roll Animation    |                                                                                                               |
| _MidiQOL Integration (Experimental)_  |                                |                                      **Pro** Settings - Requires MidiQOL                                      |
| Enable MidiQOL Integration            |        &#9745;&#65039;         |                                                                                                               |
| _JB2A/Otigon AA Integration_          |                                |                       **Pro** Settings - Requires JB2A and Automated Animations modules                       |
| Enable Automated Animations           |        &#9745;&#65039;         |                                                                                                               |
| _Discord Integration_                 |                                |                                               **Pro** Settings                                                |
| Enable Discord Integration            |        &#9745;&#65039;         |                                                                                                               |
| Display Source                        |        &#9745;&#65039;         |                                                                                                               |
| Display Dice Set Image                |        &#9745;&#65039;         |                                                                                                               |
| Discord Wbhook URL                    |             {URL}              |                                                                                                               |
| _Item and Spell Descriptions_         |                                |                                               **Pro** Settings                                                |
| Enable Description Cards              |        &#9745;&#65039;         |                                                                                                               |
| Enable Links to D&D Beyond            |        &#9745;&#65039;         |                                                                                                               |
| Enable Links to Item/Spell Sheet      |        &#9745;&#65039;         |                                                                                                               |
| **Debug**                             |                                |                                                                                                               |
| Debug Mode                            |           &#128306;            |                                                                                                               |

### [D&D Beyond Importer](https://www.foundryvtt-hub.com/package/ddb-importer/) :parking: v2.9.51

Integrate your dndbeyond.com content: Characters, Spells, Items, Monsters, Adventures, Encounters, Classes, and Races. Quickly integrates your content into compendiums and import your characters from D&DBeyond (and sync their status back at the end of your game!)

Patreon subscription optional, however you need it to import Monsters and Adventures and Classes.

| Setting                                     |            My Setting             |                                        Comments                                        |
| ------------------------------------------- | :-------------------------------: | :------------------------------------------------------------------------------------: |
| **Core Setup**                              |                                   | I recommend using the DDB Data Grabber extension to fill out the top several settings. |
| Player Character Images                     |   [data] ddb-images/characters    |                                                                                        |
| Other Images                                |      [data] ddb-images/other      |                                                                                        |
| Avatar Frame Images                         |     [data] ddb-images/frames      |                                                                                        |
| Use local storage                           |          &#9745;&#65039;          |                                                                                        |
| **Compendiums**                             |                                   |                                                                                        |
| Backgrounds                                 |      [world] DDB Backgrounds      |                                                                                        |
| Classes                                     |        [world] DDB Classes        |                                                                                        |
| Subclasses                                  |      [world] DDB Subclasses       |                                                                                        |
| Class Features                              |    [world] DDB Class Features     |                                                                                        |
| Feats                                       |         [world] DDB Feats         |                                                                                        |
| Items                                       |         [world] DDB Items         |                                                                                        |
| Monsters                                    |       [world] DDB Monsters        |                                                                                        |
| Races                                       |         [world] DDB Races         |                                                                                        |
| Racial Traits                               |     [world] DDB Racial Traits     |                                                                                        |
| Spells                                      |        [world] DDB Spells         |                                                                                        |
| Tables                                      |        [world] DDB Tables         |                                                                                        |
| Override                                    |       [world] DDB Override        |                                                                                        |
| Create default compendiums if missing?      |          &#9745;&#65039;          |                                                                                        |
| **Dynamic DDB Update Setup**                |                                   |                    ALPHA! Only availble to higher tier supporters.                     |
| GM USer used for Syncing                    |          {Your Account}           |                                                                                        |
| Enable Dynamic Sync?                        |             &#128306;             |                                                                                        |
| Currency?                                   |             &#128306;             |                                                                                        |
| Hit Points?                                 |             &#128306;             |                                                                                        |
| Hit Dice?                                   |             &#128306;             |                                                                                        |
| Action Usage?                               |             &#128306;             |                                                                                        |
| Inspiration?                                |             &#128306;             |                                                                                        |
| Exhaustion?                                 |             &#128306;             |                                                                                        |
| Death Saves?                                |             &#128306;             |                                                                                        |
| Spells Prepared?                            |             &#128306;             |                                                                                        |
| Spell Slots?                                |             &#128306;             |                                                                                        |
| Spell Policy?                               |             &#128306;             |                                                                                        |
| Equipment?                                  |             &#128306;             |                                                                                        |
| XP?                                         |             &#128306;             |                                                                                        |
| **General Settings**                        |                                   |                                                                                        |
| Log Level                                   |               INFO                |                                                                                        |
| Munch Button at the Top?                    |             &#128306;             |                                                                                        |
| Adventure Import Path                       |     [data] adventures/import      |                                                                                        |
| Adventure Upload Path                       |   [data] ddb-images/adventures    |                                                                                        |
| Adventure Misc Upload Path                  | [data] ddb-images/adventures/misc |                                                                                        |
| Monster D&D Beyond Link in Title Bar?       |             &#128306;             |                                                                                        |
| Character DDB-Importer Button in Title Bar? |             &#128306;             |                                                                                        |
| Make Title Bar Icon White?                  |             &#128306;             |                                                                                        |
| Show the Show Image to Players Button?      |          &#9745;&#65039;          |                                                                                        |
| Restrict to Trusted Users                   |             &#128306;             |                                                                                        |
| Allow All Users to Sync to DDB?             |             &#128306;             |                                                                                        |
| Use Full Source Book Name?                  |          &#9745;&#65039;          |                                                                                        |
| Use Damage Type Hints?                      |          &#9745;&#65039;          |                                                                                        |
| Add Restrictions to Damage Hint?            |          &#9745;&#65039;          |                                                                                        |
| Embed macros in Items?                      |          &#9745;&#65039;          |                                                                                        |
| Compendium Folders Style (Monsters)         |    Creature Type, e.g. Undead     |                                                                                        |
| Compendium Folders Style (Spells)           |          School of Magic          |                                                                                        |
| Compendium Folders Style (Items)            |             Item Type             |                                                                                        |

### [Day Night Lights](https://www.foundryvtt-hub.com/package/daynightlights/) v1.0.3

A controller for adding a secondary light option that will automatically be updated when changing between day & night lighting conditions.

### [DF Architect](https://www.foundryvtt-hub.com/package/df-architect/) v3.3.1

This module provides many many many new Quality of Life features to the Foundry Walls and Lighting layers. As well as a few extras that are just fun to have and make life a little easier.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/packages/lib-wrapper), [Lib - Color Settings](https://www.foundryvtt-hub.com/packages/colorsettings)

| Setting                                      |    My Setting     | Comments |
| -------------------------------------------- | :---------------: | :------: |
| Flattened Tiles Images Folder                |     architect     |          |
| Lights: Crosshair Color                      |     #ffffffff     |          |
| Lights: Alternative Crosshair Color          |     #ff5500ff     |          |
| Show Alternative Grid Snap Toggle            |     &#128306;     |          |
| Place on Main Control Bar                    |     &#128306;     |          |
| Allow Players to Capture Canvas              |     &#128306;     |          |
| Tile Config: Show Controls for Animation     |  &#9745;&#65039;  |          |
| Show Object Counters                         |  &#9745;&#65039;  |          |
| Quick Swap Layer 1                           |   Wall Controls   |          |
| Quick Swap Layer 2                           | Lighting Controls |          |
| Quick Wall Change Key                        |     CTRL Key      |          |
| Wall Gap Filler Distance                     |         5         |          |
| Wall Drop Auto-Snap                          |        24         |          |
| Allow orinetation Invert on Unselected Walls |     &#128306;     |          |

### [DF Chat Enhancements](https://www.foundryvtt-hub.com/package/df-chat-enhance/) v3.6.1

Brings a new Chat Archive that lets you save your current chat log to an archive and keep the chat clean between sessions. Gives an option to replace the Roll Type dropdown menu with a set of 4 buttons. This makes switching rolls much more efficient and provides a better visual indicator for what roll you're in.
Dependencies: [Library: Chat Commands](https://www.foundryvtt-hub.com/package/_chatcommands/), [LibWrapper](https://www.foundryvtt-hub.com/packages/lib-wrapper)

| Setting                                               |           My Setting            |                     Comments                      |
| ----------------------------------------------------- | :-----------------------------: | :-----------------------------------------------: |
| Folder to save Chat Archives                          | worlds/{worldname}/chat-archive |                                                   |
| **Adventure Log Settings**                            |                                 |                                                   |
| Journal to use as the Adventure Log                   |       Party Adventure Log       | You will need to create this journal entry first. |
| Clear All Contents of the Journal                     |            &#128306;            |                                                   |
| Journal to use as the GM Only Log                     |        GM Adventure Log         | You will need to create this journal entry first. |
| Clear All Contents of the Journal                     |            &#128306;            |                                                   |
| **General Settings**                                  |                                 |                                                   |
| Chat Archive: Hide the Export Chat Log Button         |            &#128306;            |                                                   |
| Roll Buttons: Replace Roll Type Selector With Buttons |         &#9745;&#65039;         |                                                   |
| Roll Buttons: Use Button Style on All Chat Buttons    |         &#9745;&#65039;         |                                                   |
| Chat Merge: Enable Chat Message Merging               |         &#9745;&#65039;         |                                                   |
| Chat Merge: Show Message Headers                      |            &#128306;            |                                                   |
| Group By Speaker                                      |         &#9745;&#65039;         |                                                   |
| Chat Merge: How to Handle Rolls When Merging          |     Merge With Other Rolls      |                                                   |
| Chat Merge: Show Merged Message Divider               |            &#128306;            |                                                   |
| Chat Merge: Show Hover Shadow                         |         &#9745;&#65039;         |                                                   |
| Chat Merge: Maximum Period of Time                    |               10                |                                                   |
| Enable Chat Scroll Improvements                       |         &#9745;&#65039;         |                                                   |
| Scroll to Bottom When You Send a Message              |         &#9745;&#65039;         |                                                   |
| Enable Whisper Name List Truncation                   |         &#9745;&#65039;         |                                                   |
| Whisper Truncation Character Limit                    |               50                |                                                   |
| Tint Chat Message Background                          |         &#9745;&#65039;         |                                                   |
| Adventure Log: Enable                                 |         &#9745;&#65039;         |                                                   |
| Adventure Log: Set Adventure Log GM Only              |            &#128306;            |                                                   |
| Adventure Log: (If GM Only) All Logs are Whispers     |            &#128306;            |                                                   |
| Adventure Log: Include Current Time                   |         &#9745;&#65039;         |                                                   |
| Adventure Log: Post Logs to Chat                      |         &#9745;&#65039;         |                                                   |
| Adventure Log: Sort Newest on Top                     |            &#128306;            |                                                   |
| Adventure Log: Use SimpleCalendar Timestamps          |         &#9745;&#65039          |                                                   |
| Chat Edit: Enable Markdown                            |         &#9745;&#65039;         |                                                   |
| Chat Edit: Allow Message Editing                      |         &#9745;&#65039;         |                                                   |
| Chat Edit: GM Can Edit All                            |            &#128306;            |                                                   |
| Chat Edit: Ignore Messages Containing HTML            |            &#128306;            |                                                   |

### [DF Quality of Life](https://www.foundryvtt-hub.com/package/df-qol/) v1.7.3

Adds various Quality of Life improvements:

- Auto-Target Tokens with Template
- D&D 5e Style Templates
- Token Locking
- Quick Table Rolling
- Auto-Focus Text Box When Creating Entities
- Custom Folder Text Colours
- Vehicle Cargo Capacity Unit
- Day/Night Transition Progress and Duration
- Better Toggle Styling

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/packages/lib-wrapper)

| Setting                                           |   My Setting    | Comments |
| ------------------------------------------------- | :-------------: | :------: |
| Enable Roll Table Menu Option to Quick Roll       | &#9745;&#65039; |          |
| Enable Folder Text Color                          | &#9745;&#65039; |          |
| Enable Auto-Focusing Text Field In Entity Dialogs | &#9745;&#65039; |          |
| Enable Better Scene Control Toggle Highlight      | &#9745;&#65039; |          |
| Display Day/Night Transition Progress             |    &#128306;    |          |
| Day/Night Transition Duration (Seconds)           |       10        |          |
| Enable Unit of Weight for Vehicles                | &#9745;&#65039; |          |
| Token Locking: Enable                             | &#9745;&#65039; |          |
| Token Locking: Ignore GM                          | &#9745;&#65039; |          |

### [DF Scene Enhancement](https://www.foundryvtt-hub.com/package/df-scene-enhance/) v3.5.1

A few enhancements to scene management for players and GMs.

- Navigate to scene instead of displaying config when clicking a scene on the Scenes Tab.
- Display dialog with Config and Navigate options when clicking scene link in Journal Entry.
- Set custom Thumbnail for scene
- Lock current/custom ratio of scene dimensions when resizing
- Uniform scale scene dimensions by a multiplier
- Allow players to see the Scenes Tab

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/packages/lib-wrapper)

| Setting                                                       |   My Setting    | Comments |
| ------------------------------------------------------------- | :-------------: | :------: |
| Navigate When a Scene Reference is Clicked in a Journal       | &#9745;&#65039; |          |
| Execute Scene Link Immediately if Only One Option is Avaiable |    &#128306;    |          |
| Navigate When Scene is Clicked                                | &#9745;&#65039; |          |
| Enable Scene Tab for Players                                  | &#9745;&#65039; |          |
| Display Full Names in Nav Bar for GM                          | &#9745;&#65039; |          |

### [DF Settings Clarity](https://www.foundryvtt-hub.com/package/df-settings-clarity/) :part_alternation_mark: v3.2.1

This simple module provides a measure of clarity to the module configurations. It adds a tag to the start of each module settings' name that indicates if it is a `world` (GM Only) or a `client` (Per User) setting. This is very useful for GM's to know which settings the users can see, and which ones only the GM can see. Also it can help clear up confusion when the GM is helping a player setup their settings.

### [DF Template Enhancements](https://www.foundryvtt-hub.com/package/df-templates/) v1.2.1

Enhanced templates for different types of grid targetting.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/packages/lib-wrapper)

| Setting                                                    |   My Setting    | Comments |
| ---------------------------------------------------------- | :-------------: | :------: |
| **Template Measures Settings**                             |                 |          |
| Circle Template                                            |  Center Point   |          |
| Cone Template                                              |     Touches     |          |
| Rectangle Template                                         |     Touches     |          |
| Ray Template                                               |     Touches     |          |
| **General Settings**                                       |                 |          |
| Templates Auto-Target Tokens                               |     Always      |          |
| Auto-Target Gridless Resolution                            |        3        |          |
| Auto-Target Gridless Percentage                            |        0        |          |
| Preview Template Highlight Targeting                       | &#9745;&#65039; |          |
| Snap Templates to Grid Intersections                       | &#9745;&#65039; |          |
| Template Angle Snap Point Count                            |       24        |          |
| Small Rotation Multiplier                                  |        3        |          |
| Fix Square Template Rotation                               | &#9745;&#65039; |          |
| [DEBUG ONLY] Display Auto-Target Point Gridsand Test Grids |    &#128306;    |          |

### [DFreds Convenient Effects](https://www.foundryvtt-hub.com/package/dfreds-convenient-effects/) v2.6.1

Do you struggle to remember what all those conditions and spells actually do to players and NPCs? Or how they interact? Consider this. Your player just rolled, but you forgot that they were invisible and should have rolled with advantage. But wait a minute, the enemy is also invisible! So now it cancels out. "Oh drats," you think to yourself to further prove this crazy point, "the player also has Bless cast on them! Has that expired yet??"

Sure, you could figure all that out... if you wanna be lame. Or you could be cool and just use this module.

Dependencies: [Socketlib](https://www.foundryvtt-hub.com/packages/socketlib)

| Setting                    |    My Setting    | Comments |
| -------------------------- | :--------------: | :------: |
| Chat Message Permissions   |       None       |          |
| Controls Permission        |   Game Master    |          |
| Integrate with ATL         | &#9745;&#65039;  |          |
| Integrate with Token Magic | &#9745;&#65039;  |          |
| Modify Status Effects      |     Replace      |          |
| Prioritize Targets         |    &#128306;     |          |
| Show Chat Message Effect   | On Add or Remove |          |
| Show Nested Effects        | &#9745;&#65039;  |          |

### [DFreds Effects Panel](https://www.foundryvtt-hub.com/package/dfreds-effects-panel/) v1.5.1

Do you find it annoying to have to open up the character sheet just to see what effects are applied? Is it annoying to delete them or edit them on the fly? Well, this module makes that slightly less annoying!

On selecting a token on the canvas, this module shows a little panel in the top right of all the currently active effects for that actor. From here, you can do the following:

You can hover over the icon to see the name, description (if using DFreds Convenient Effects), and the time remaining. This integrates nicely with modules that handle time management such as Simple Calendar to show the time until it expires. When an effect runs out of time, it will be labeled with "Expired".

You can double click the icon to immediately open the configuration sheet for that effect.

You can right click the icon to immediately delete the effect.

| Setting                                |     My Setting     | Comments |
| -------------------------------------- | :----------------: | :------: |
| Show Disabled Effects                  |  &#9745;&#65039;   |          |
| Show Passive Effects                   |     &#128306;      |          |
| Passive Effects Right-Click Behavior   |      Disable       |          |
| Temporary Effects Right-Click Behavior | Delete With Dialog |          |
| View Permission                        |       Player       |          |
| View Details Permission                |       Player       |          |

### [Dice So Nice!](https://www.foundryvtt-hub.com/package/dice-so-nice/) :part_alternation_mark: v4.5.1

Adds the ability to show a 3D dice simulation when a roll is made.

| Setting                                                                           |                My Setting                 |                                     Comments                                     |
| --------------------------------------------------------------------------------- | :---------------------------------------: | :------------------------------------------------------------------------------: |
| **My Dice Settings**                                                              | These settings are completely subjective. | If you want my setup, find the import file in the Configuration Files directory. |
| _Appearance_                                                                      |                                           |                                                                                  |
| Enable 3D Dice                                                                    |              &#9745;&#65039;              |                                                                                  |
| Enable Extra Dice Customization                                                   |                 &#128306;                 |                                                                                  |
| Dice Presets (faces)                                                              |                 Standard                  |                                                                                  |
| Theme                                                                             |                Blood Moon                 |                                                                                  |
| Texture                                                                           |                 Dark Ooze                 |                                                                                  |
| Material                                                                          |                   Metal                   |                                                                                  |
| Font                                                                              |                   Fire                    |                                                                                  |
| Label Color                                                                       |                  #FFFFFF                  |                                                                                  |
| Dice Color                                                                        |                  #1f84ef                  |                                                                                  |
| Outline Color                                                                     |                  #1f84ef                  |                                                                                  |
| Edge Color                                                                        |                  #1f84ef                  |                                                                                  |
| _Preferences_                                                                     |                                           |                                                                                  |
| Automatically Hide                                                                |              &#9745;&#65039;              |                                                                                  |
| Millisecs Before Hiding                                                           |                   2000                    |                                                                                  |
| Hide FX                                                                           |                 Fade Out                  |                                                                                  |
| Sound Effects                                                                     |              &#9745;&#65039;              |                                                                                  |
| Mute all sounds for GM/Blind/Self Rolls                                           |                 &#128306;                 |                                                                                  |
| Sound Volume                                                                      |                    0.5                    |                                                                                  |
| Table Surface for Sounds                                                          |                Wood Table                 |                                                                                  |
| Auto Scale?                                                                       |              &#9745;&#65039;              |                                                                                  |
| Manual Scale                                                                      |                    75                     |                                                                                  |
| Animation Speed                                                                   |                  Normal                   |                                                                                  |
| 3D Layer Position                                                                 |                Over Sheets                |                                                                                  |
| Throwing Force                                                                    |                  Medium                   |                                                                                  |
| Override Your Dice Appearance When a Theme is Passed as a Flavor (ex: 1d20[acid]) |              &#9745;&#65039;              |                                                                                  |
| Enable Immersive Darkness Mode (Dice React to Scene Lighting)                     |              &#9745;&#65039;              |                                                                                  |
| _Special Effects_                                                                 |                                           | If you want my setup, find the import file in the Configuration Files directory. |
| Show Other Players Special Effects                                                |              &#9745;&#65039;              |                                                                                  |
| - d20 = 1 = Particles: Magic Vortex                                               |                                           |                                                                                  |
| - d20 = 1 = Sound: Epic Fail                                                      |                                           |                                                                                  |
| - d20 = 20 = Particles: Blaze                                                     |                                           |                                                                                  |
| - d20 = 20 = Sound: Epic Win                                                      |                                           |                                                                                  |
| - d100 = 1 = Particles: Magic Vortex                                              |                                           |                                                                                  |
| - d100 = 1 = Sound: Epic Fail                                                     |                                           |                                                                                  |
| - d100 = 100 = Particles: Blaze                                                   |                                           |                                                                                  |
| - d100 = 100 = Sound: Epic Win                                                    |                                           |                                                                                  |
| _Performance_                                                                     |                                           |                                                                                  |
| Image Quality                                                                     |                   High                    |                                                                                  |
| Realistic Lighting                                                                |              &#9745;&#65039;              |                                                                                  |
| Shadows Quality                                                                   |                   High                    |                                                                                  |
| Glowing Lights                                                                    |              &#9745;&#65039;              |                                                                                  |
| Anti-Aliasing                                                                     |       MSAA (Hardware, Recommended)        |                                                                                  |
| UHD Resolution Support (4k, Retina...)                                            |              &#9745;&#65039;              |                                                                                  |
| **General Settings**                                                              |                                           |                                                                                  |
| Max Number of Dice                                                                |                    20                     |                                                                                  |
| Global Animation Speed                                                            |              Player's Choice              |                                                                                  |
| Simultaneous Rolls are Merged                                                     |              &#9745;&#65039;              |                                                                                  |
| Same-Message Rolls are Merged                                                     |              &#9745;&#65039;              |                                                                                  |
| Dice Can Be Flipped                                                               |              &#9745;&#65039;              |                                                                                  |
| Disabled During Combat                                                            |                 &#128306;                 |                                                                                  |
| Disbled for Initiative Rolls                                                      |                 &#128306;                 |                                                                                  |
| Display Chat Message Immediately                                                  |                 &#128306;                 |                                                                                  |
| Enable 3D Dice on Roll Tables                                                     |              &#9745;&#65039;              |                                                                                  |
| Enable 3D Dice on Inline Rolls                                                    |              &#9745;&#65039;              |                                                                                  |
| Disable 3D Dice on NPC Rolls                                                      |                 &#128306;                 |                                                                                  |
| Allow Interaction With Rolled Dice                                                |              &#9745;&#65039;              |                                                                                  |
| Show Ghost Dice for Hidden Rolls                                                  |                 &#128306;                 |                                                                                  |

### [Dice So Nice - Weighted Companion Cube Dice Set](https://www.foundryvtt-hub.com/package/wcube/) v1.0.4

Adds a set of 3D dice in the style of Aperture Science Weighted Companion Cube to Dice-So-Nice.

Dependencies: [Dice So Nice!](https://www.foundryvtt-hub.com/package/dice-so-nice/)

### [Dice Tray](https://www.foundryvtt-hub.com/package/dice-calculator/) v1.5.2

This module adds a dice tray below the chat message area and turns the d20 icon near the chat prompt into a clickable link that opens up a new dice calculator dialog. The dice calculator includes buttons for dice, numbers, attributes for the selected token, and simple math. In addition, it includes support shorthand in for inline dice rolls, such as `@abil` and `@attr` instead of `@abilities` and `@attributes`, respectively.

| Setting                |   My Setting    | Comments |
| ---------------------- | :-------------: | :------: |
| Enable Dice Calculator |    &#128306;    |          |
| Enable Dice Tray       | &#9745;&#65039; |          |
| Override System        |      DnD5e      |          |
| Enable d6 Pips         |    &#128306;    |          |

### [dlopen](https://www.foundryvtt-hub.com/package/dlopen/) v1.0

A librarv module contributed by The Forge for dynamicallv loading library dependencies into Foundry upon use.

### [DnD5 Cheatsheet](https://www.foundryvtt-hub.com/package/dnd5-cheatsheet/) v0.0.7

This module aims to provide a fast and concise DnD5 Cheatsheet. With easy access buttons (can be disabled in settings).

Available in English and French.

Dependencies: [Babele](https://www.foundryvtt-hub.com/package/babele/)

| Setting                      |                                                                My Setting                                                                 | Comments |
| ---------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------: | :------: |
| **Control Buttons**          |                                                                                                                                           |          |
| - Combat                     |   Title: dnd5-cheatsheet.controls.combat, Icon: fas fa-fist-raised, Compendium Entry: dnd5-cheatsheet.dnd5-cheatsheet.96yLLrkGd9Wdgrh4    |          |
| - Range-Combat               | Title: dnd5-cheatsheet.controls.range-combat, Icon: far fa-dot-circle, Compendium Entry: dnd5-cheatsheet.dnd5-cheatsheet.sWPEV6eJ09WE5hkZ |          |
| - Move                       |      Title: dnd5-cheatsheet.controls.move, Icon: fas fa-running, Compendium Entry: dnd5-cheatsheet.dnd5-cheatsheet.nXtpaIsrorRtpVLh       |          |
| - Magic-Item                 |    Title: dnd5-cheatsheet.controls.magic-item, Icon: fas fa-tools, Compendium Entry: dnd5-cheatsheet.dnd5-cheatsheet.p5phMKTqf387Wco5     |          |
| **General Settings**         |                                                                                                                                           |          |
| Display Quick Access Buttons |                                                              &#9745;&#65039;                                                              |          |

### [DnD5e Drag Ruler Integration](https://www.foundryvtt-hub.com/package/elevation-drag-ruler/) v1.6.3

Adds a DnD5e speedprovider for Drag Ruler to manually or automatically pick between different movement speeds based on elevation and/or terrain from Enhanced Terrain Layer.

Dependencies: [Drag Ruler](https://www.foundryvtt-hub.com/package/drag-ruler/)

### [DnD5e Helpers](https://www.foundryvtt-hub.com/package/dnd5e-helpers/) :part_alternation_mark: v4.1.3

Little helpers for little 5e tasks.

- Automatic Wild Magic Surge
- Automatic combat action management
- Legendary Action reset on start of turn
- Legendary and Lair Action reminder prompts
- Recharge abilities on start of turn
- 5/5/5 Diagonal Template Scaling
- Cover Calculator with cover bonus automation
- Optional expiry of AE's on rests
- Great Wound Detection
- Automatic specific proficiencies detection
- Regeneration reminder
- Undead Fortitude reminder
- Open Wounds

| Setting                                                        |              My Setting              | Comments |
| -------------------------------------------------------------- | :----------------------------------: | :------: |
| **System Helpers**                                             |                                      |          |
| Status Effect Scale                                            |                  1                   |          |
| Auto Adjust Templates to 5e Grids                              |         No Temlpate Scaling          |          |
| **Cover Calculation Configuration**                            |                                      |          |
| _System Helpers_                                               |                                      |          |
| Cover Computation Mode                                         | Center Point Vision (Foundry Vision) |          |
| Compute Cover on Target                                        |           &#9745;&#65039;            |          |
| Tile Cover                                                     |              &#128306;               |          |
| Token Cover                                                    |              &#128306;               |          |
| Cover Button Tint                                              |               Dark Red               |          |
| Cover Application                                              |               Disabled               |          |
| Whisper Cover Report to Self                                   |              &#128306;               |          |
| Hide GM Info from Cover Report                                 |           &#9745;&#65039;            |          |
| _Combat Helpers_                                               |                                      |          |
| Remove Cover on Turn End                                       |              &#128306;               |          |
| _Misc_                                                         |                                      |          |
| Debug Line of Sight                                            |              &#128306;               |          |
| **NPC Feature Helpers**                                        |                                      |          |
| Automatic Regeneration                                         |              &#128306;               |          |
| Effect Name to Block Regeneration                              |               No Regen               |          |
| Start of Turn Legendary Action Reset                           |              &#128306;               |          |
| Legendary Action Prompt                                        |              &#128306;               |          |
| Lair Action Prompt                                             |              &#128306;               |          |
| _Undead Fortitude Helpers_                                     |                                      |          |
| Undead Fortitude Checks                                        |              No checks               |          |
| Undead Fortitude - Damage Type                                 |               Radiant                |          |
| Undead Fortitude Name                                          |           Undead Fortitude           |          |
| Undead Fortitude - DC                                          |                  5                   |          |
| **PC Feature Helpers**                                         |                                      |          |
| _Wild Magic Helpers_                                           |                                      |          |
| Wild Magic Auto-Detect                                         |               Disabled               |          |
| Wild Magic Surge Table Name                                    |          Wild Magic Surges           |          |
| Tides of Chaos Feature Name                                    |            Tides of Chaos            |          |
| Recharge Tides of Chaos on Surge                               |              &#128306;               |          |
| Hide Wild Magic from Players                                   |              &#128306;               |          |
| Table Roll Mode                                                |           Private GM Roll            |          |
| **Combat Helpers**                                             |                                      |          |
| Remove Targets on Turn End                                     |              &#128306;               |          |
| Enable Combat Action Management                                |               Disabled               |          |
| Display Used Actions as Status Effect                          |            Only Reactions            |          |
| Enable Action Mangement HUD                                    |               Enabled                |          |
| Automatically Roll Any Uncharged Abilitiees With a d6 Recharge |                 Off                  |          |
| Hide the Results of Ability Recharge Rolls                     |              &#128306;               |          |
| **Variant Rule: Great Wounds**                                 |                                      |          |
| _Systm Helpers_                                                |                                      |          |
| Great Wound                                                    |              &#128306;               |          |
| Great Wound Name Replacement                                   |             Great Wound              |          |
| Great Wound Table                                              |                                      |          |
| Hide GM Info From Greate Wound and Open Wound Report           |              &#128306;               |          |
| _Combat Helpers_                                               |                                      |          |
| Trigger: Percent of Max HP                                     |                  50                  |          |
| Trigger: Flat Damage                                           |                  0                   |          |
| Great Wound Save DC                                            |                  15                  |          |
| Apply Effects to Great Wounds                                  |               Disabled               |          |
| **Variant Rule: Open Wounds**                                  |                                      |          |
| _System Helpers_                                               |                                      |          |
| Open Wound - HP at 0                                           |              &#128306;               |          |
| Open Wound - HP at 0 From a Great Wound                        |              &#128306;               |          |
| Open Wound Feature Name                                        |              Open Wound              |          |
| Open Wound Table                                               |                                      |          |
| Apply Effects to Open Wound                                    |              &#128306;               |          |
| _Combat Helpers_                                               |                                      |          |
| Open Wound for PC's Only                                       |              &#128306;               |          |
| Open Wound - Death Saves                                       |                  0                   |          |
| Open Wound - Crits                                             |              &#128306;               |          |
| **Misc**                                                       |                                      |          |
| Debug                                                          |              &#128306;               |          |

### [Dnd5e Helpers Cover Expansion](https://www.foundryvtt-hub.com/package/dnd5e-helpers-cover-expansion/) v0.2.01

Auto sets token cover value based on their actors size for DnD5e Helpers. Changes cover level based on death/unconsciousness as well.

Dependencies: [DnD5e Helpers](https://www.foundryvtt-hub.com/package/dnd5e-helpers/)

| Setting                                        |   My Setting   | Comments |
| ---------------------------------------------- | :------------: | :------: |
| Token Sizes to Not Provide Cover               |    tiny,sm     |          |
| 3/4 Cover Token Sizes                          |     med,lg     |          |
| Token Sizes to Not Provide Cover When at 0 HP  | tiny,sm,med,lg |          |
| Token Sizes to Provide Half Cover When at 0 HP |      huge      |          |
| Token Sizes to Provide 3/4 Cover When at 0 HP  |      grg       |          |
| Token Sizes to Provide Full Cover When at 0 HP |                |          |

### [Drag Ruler](https://www.foundryvtt-hub.com/package/drag-ruler/) :part_alternation_mark: v1.12.8

This module shows a ruler when you drag a token or measurement template to infrom you how far you've dragged it from it's start point. Additionally, if you're using a grid, the spaces the token will travel though will be colored depending on your tokens speed. By default three colors are being used: green for spaces that your token can reach by walking normally, spaces that can only be reached by dashing will be colored yellow and spaces that cannot be reached with the token's speed will be colored red. If you're using a gridless map the ruler color will change to convey this information.

Dependencies: [Socketlib](https://www.foundryvtt-hub.com/package/socketlib/)

| Setting                               |             My Setting              | Comments |
| ------------------------------------- | :---------------------------------: | :------: |
| **Speed Provider Settings**           |                                     |          |
| Speed Settings Provider               | Module DnD5e Drag Ruler Integration |          |
| Use Elevation                         |           &#9745;&#65039;           |          |
| Elevate Flying                        |           &#9745;&#65039;           |          |
| Force Flying                          |           &#9745;&#65039;           |          |
| Force Swimming                        |           &#9745;&#65039;           |          |
| Force Burrowing                       |           &#9745;&#65039;           |          |
| Hide "Switch Speed" Button            |              &#128306;              |          |
| Restrict the "Switch Speed" Button    |               Player                |          |
| Hide "Toggle Terrain" Button          |              &#128306;              |          |
| Restrict the "Toggle Terrain" Button  |               Player                |          |
| Color for Walking                     |               #00ff00               |          |
| Color for Dash Walking                |               #ffff00               |          |
| Color for Flying                      |               #00ffff               |          |
| Color for Dash Flying                 |               #ffff00               |          |
| Color for Swimming                    |               #0000ff               |          |
| Color for Dash Swimming               |               #ffff00               |          |
| Color for Burrowing                   |               #ffaa00               |          |
| Color for Dash Burrowing              |               #ffff00               |          |
| Color for CLimbing                    |               #aa6600               |          |
| Color for Dash Climbing               |               #ffff00               |          |
| Color for Unreachable                 |               #ff0000               |          |
| **General Settings**                  |                                     |          |
| Right-Click Action                    |           Delete Waypoint           |          |
| Automatically Start Measuring         |           &#9745;&#65039;           |          |
| Use Speed Based Snapping              |           &#9745;&#65039;           |          |
| Show PC Speed to Everyone             |           &#9745;&#65039;           |          |
| Show GM Ruler to Players              |              &#128306;              |          |
| Enable Movement History During Combat |           &#9745;&#65039;           |          |
| Enable Pathfinding for Players        |              &#128306;              |          |
| Pathfinding by Default                |              &#128306;              |          |

### [Dynamic Active Effects SRD](https://www.foundryvtt-hub.com/package/dynamic-effects-srd/) :part_alternation_mark: v6.0.01

A compendium pack of DnD 5e SRD items, spells and features all configured specifically for use with Dynamic Active Effects. Now includes items configured with Magic Items module too. Currently contains all spells and items in the SRD pack. Easily automate your shield spells; barbarian rages; cloak of protection and many more (as they are added).

Dependencies: [Dynamic Effects Using Active Effects](https://www.foundryvtt-hub.com/package/dae/)
Optional Dependencies: [Midi-QOL](https://www.foundryvtt-hub.com/package/midi-qol/), [Advanced Macros](https://www.foundryvtt-hub.com/package/advanced-macros/)

### [Dynamic Effects Using Active Effects](https://www.foundryvtt-hub.com/package/dae/) :part_alternation_mark: v0.10.18

You do not need to use DAE for most of the active effect funcitonality to work. You might choose to use it if you want one or more or of:

- Automatic Armor calculation
- The ability to reference other fields in active effect calculations (core only allows numbers or strings)
- The ability to change more fields on the characters

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/libwrapper/), [Socketlib](https://www.foundryvtt-hub.com/package/socketlib/)

| Setting                                          |   My Setting    | Comments |
| ------------------------------------------------ | :-------------: | :------: |
| Non-Trasfer Effects Require Item Targets (dnd5e) | &#9745;&#65039; |          |
| Use the DAE Active Effects Config Sheet?         | &#9745;&#65039; |          |
| No Duplicate Damage Macros                       |    &#128306;    |          |
| Expire Timed Effects in Real Time                |    &#128306;    |          |
| Display Results of Inline Rolls                  |    &#128306;    |          |
| Confirm Effect Deletions                         |    &#128306;    |          |
| DAE Title Bar                                    | &#9745;&#65039; |          |
| Just Icon                                        | &#9745;&#65039; |          |
| Load DAE for Untested Game Systems               |    &#128306;    |          |
| Amount of Debug to Show                          |      None       |          |
| Disable All Active Effect Processing             |    &#128306;    |          |
| Include Active Effects in Special Traits Display |    &#128306;    |          |

### [Enhanced Terrain Layer](https://www.foundryvtt-hub.com/package/enhanced-terrain-layer/) v1.0.41

Adds a Terrain Layer to Foundry that can be used by other modules to calculate difficult terrain.

| Setting                        |   My Setting    | Comments |
| ------------------------------ | :-------------: | :------: |
| **Edit Colors**                |                 |          |
| Global Color                   |     #ffffff     |          |
| _Envrionments_                 |                 |          |
| Arctic                         |     #ffffff     |          |
| Coast                          |     #00ffff     |          |
| Desert                         |     #ffdd00     |          |
| Forest                         |     #00ff88     |          |
| Grassland                      |     #00ff33     |          |
| Jungle                         |     #00991f     |          |
| Mountain                       |     #808080     |          |
| Swamp                          |     #805700     |          |
| Underdark                      |     #cc80ff     |          |
| Urban                          |     #bababa     |          |
| Water                          |     #80bdff     |          |
| _Obstacles_                    |                 |          |
| Crowd                          |     #ff0000     |          |
| Current                        |     #004cff     |          |
| Furniture                      |     #805700     |          |
| Magic                          |     #ae00ff     |          |
| Plants                         |     #008002     |          |
| Rubble                         |     #808080     |          |
| **General Settings**           |                 |          |
| Terrain Opacity                |        1        |          |
| Draw Border                    | &#9745;&#65039; |          |
| Terain Image                   |    Diagnoal     |          |
| Show Text                      |    &#128306;    |          |
| Show Icons                     | &#9745;&#65039; |          |
| Show on Drag                   | &#9745;&#65039; |          |
| Only Show Active Terrain       |    &#128306;    |          |
| Tokens Cause Difficult Terrain |    &#128306;    |          |
| Dead Cause Difficult Terrain   |    &#128306;    |          |
| Use Additional Obstacles       |    &#128306;    |          |
| Minimum Cost                   |       0.5       |          |
| Maximum Cost                   |        4        |          |

### [Entice with Dice so Nice](https://www.foundryvtt-hub.com/package/entice-with-dice-so-nice/) v0.0.16

This module works as an addon to Dice so Nice, adding more textures to give users more customization options for their dice.

Dependencies: [Dice So Nice!](https://www.foundryvtt-hub.com/package/dice-so-nice/)

### [Filepicker Improvements](https://www.foundryvtt-hub.com/package/foundry-filepicker-favorites/) v1.2.5

A simple module to add favorite directories and a search to the file picker

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/libwrapper/)

| Setting                                                        | My Setting | Comments |
| -------------------------------------------------------------- | :--------: | :------: |
| **Favorites Settings**                                         |            |          |
| - Core Icons:Core Data:icons/                                  |            |          |
| - Characters:My Assets Library:custom/icons/characters/        |            |          |
| - Creatures:My Assets Library:custom/icons/creatures/          |            |          |
| - Items:My Assets Library:custom/icons/items/                  |            |          |
| - Spirits:My Assets Library:custom/icons/spirits/              |            |          |
| - Ambience:My Assets Library:moulinette/sounds/custom/Ambience |            |          |
| - Music:My Assets Library:moulinette/sounds/custom/Music       |            |          |
| - Sound FX:My Assets Library:moulinette/sounds/custom/SFX      |            |          |
| **Exclude Search Directories**                                 |            |          |
| - None                                                         |            |          |
| **General Settings**                                           |            |          |
| Maximum Number of Results in Search                            |    500     |          |

### [Find the Culprit! A Module Debugging Helper](https://www.foundryvtt-hub.com/package/find-the-culprit/) v1.4.0

This module helps you to find a module causing issues, by automating the process of deactivating and reactivating modules. Just click the 'Find the culprit' button inside Module Management.

### [Forien's Copy Environment](https://www.foundryvtt-hub.com/package/forien-copy-environment/) v2.1.3

Allows for copying TXT/JSON list of installed and &#9745;&#65039; system/modules and their versions. Now includes option to export and import game settings.

Ricght-Click in the Settings Menu for import/export options.

### [Forien's Unidentified Items](https://www.foundryvtt-hub.com/package/forien-unidentified-items/) v0.4.1

This module aims to provide system agnostic solution to handle unidentified items and their identification.

Please refer to [Wiki](https://github.com/Forien/foundryvtt-forien-unidentified-items/wiki#usage) for full information on Usage

| Setting                                                              |   My Setting    |       Comments       |
| -------------------------------------------------------------------- | :-------------: | :------------------: |
| **Default Icons**                                                    |                 | Leave all as default |
| **Persisting Properties**                                            |                 | Leave all as default |
| **General Settings**                                                 |                 |                      |
| forien-unidentified-items.Settings.RemoveLabelButtonSheetHeader.name | &#9745;&#65039; |                      |
| Keep Original Icon                                                   |    &#128306;    |                      |
| Allow Tiering (Nesting)                                              |    &#128306;    |                      |

### [Foundry Community Macros](https://www.foundryvtt-hub.com/package/foundry_community_macros/) v9.15

This is a collection of Macros added by the FoundryVTT Community.

### [Foundry Community Tables](https://www.foundryvtt-hub.com/package/foundry_community_tables/) v9.3

This is a collection of Roll Tables added by the FoundryVTT Community.

### [Foundry VTT Anniversary Dice Set by The Rollsmith](https://www.foundryvtt-hub.com/package/trs-foundryvtt-dice-set/) v1.0.0

A special iron and lava infused dice set for Dice So Nice commissioned to celebrate Foundry Virtual Tabletop's 2-Year Anniversary.

### [FoundryVTT Arms Reach](https://www.foundryvtt-hub.com/package/foundryvtt-arms-reach/) v2.1.29

The interaction distance is measured by the distance between a token and a placeable object like door, journal, stairways, etc. to ensure a token is close enough to interact with it.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/packages/lib-wrapper)

| Setting                                                                     |   My Setting    | Comments |
| --------------------------------------------------------------------------- | :-------------: | :------: |
| Enable/Disable Arms Reach Feature                                           | &#9745;&#65039; |          |
| Notifications Failed Interactions                                           | &#9745;&#65039; |          |
| [Not Recommended] Global Maximum Interaction Distance in Tiles              |        0        |          |
| Global Maximum Interaction Distance Measurement (ft, mt, ...)               |        5        |          |
| Notifications Failed Interactions Even For GM                               |    &#128306;    |          |
| Avoid Deselects the Controlled Token When Open/Close the Door               |    &#128306;    |          |
| Use the Owned Tokens if No Token is Selected                                | &#9745;&#65039; |          |
| Enable/Disable Doors Integration                                            | &#9745;&#65039; |          |
| Maximum Door Interaction Distance Measurement (ft, mt, ...)                 |        0        |          |
| [Not Recommended] Interaction door With the Hotkey `e`                      |    &#128306;    |          |
| [Not Recommended] Interaction Door With Double Tap Delay                    |        0        |          |
| [Not Recommended] Hotkey `e` to Center Camera                               |    &#128306;    |          |
| Disable Sound of the Door if Not Reachable                                  | &#9745;&#65039; |          |
| Enable/Disable Stairways Integration                                        |    &#128306;    |          |
| Enable/Disable Notes Integration                                            |    &#128306;    |          |
| Enable/Disable Tokens Integration                                           |    &#128306;    |          |
| Enable the Distance Interaction for 'Loot Sheet'                            |    &#128306;    |          |
| Enable the Distance Interaction on Target Token With a Specific Name Prefix |      ART\_      |          |
| Set the name of the Source Token You Want to Use for the Interaction        |                 |          |
| Enable/Disable Lights Integration                                           |    &#128306;    |          |
| Enable/Disable Sounds Integration                                           |    &#128306;    |          |
| Enable/Disable Drawings Integration                                         |    &#128306;    |          |
| Enable/Disable Tiles Integration                                            |    &#128306;    |          |
| Enable/Disable Walls Integration                                            |    &#128306;    |          |
| Enable/Disable Tagger Module Integration                                    |    &#128306;    |          |
| Reset Doors and Fog Feature                                                 | &#9745;&#65039; |          |
| Enable Debugging                                                            |    &#128306;    |          |

### [Foundry VTT Content Parser](https://www.foundryvtt-hub.com/package/foundry-vtt-content-parser/) v0.2.15

Import foundry entities from external sources.

| Setting                  |   My Setting    | Comments |
| ------------------------ | :-------------: | :------: |
| Folder Depth             |        3        |          |
| Journal Importer         |    &#128306;    |          |
| Table Importer           | &#9745;&#65039; |          |
| Actor importer (5e Only) | &#9745;&#65039; |          |
| Item Importer (5e Only)  | &#9745;&#65039; |          |

### [FXMaster](https://www.foundryvtt-hub.com/package/fxmaster/) :part_alternation_mark: v2.7.0

FXMaster brings unique and configurable weather effects, filters and also special effects activated on click. And yes, you can throw fireballs.

| Setting                       |   My Setting    | Comments |
| ----------------------------- | :-------------: | :------: |
| Enable FX                     | &#9745;&#65039; |          |
| Use FXMaster Special Effects  |  Assistant GM   |          |
| Disable Effects for Everybody |    &#128306;    |          |

### [GM Notes](https://www.foundryvtt-hub.com/package/gm-notes/) :part_alternation_mark: v0.5.0

Adds the option to create Notes for Actors, Items and JournalEntrys only accessible to GMs.

| Setting                             |   My Setting    | Comments |
| ----------------------------------- | :-------------: | :------: |
| Hide GM Note Label                  | &#9745;&#65039; |          |
| Change Color When Notes are Present | &#9745;&#65039; |          |

### [GM Screen](https://www.foundryvtt-hub.com/package/gm-screen/) v3.0.1

Creates a Configurable modular grid that GMs can populate with journal entries, rollable tables, etc.

| Setting                    |   My Setting    |       Comments        |
| -------------------------- | :-------------: | :-------------------: |
| **Grid Tabs**              |                 | Completely Subjective |
| **General Settings**       |                 |                       |
| Columns                    |        4        |                       |
| Rows                       |        3        |                       |
| Display as Drawer          | &#9745;&#65039; |                       |
| Drawer Only: Right Margin  |        0        |                       |
| Drawer Only: Width         |       100       |                       |
| Drawer Only: Height        |       60        |                       |
| Drawer Only: Opacity       |      0.75       |                       |
| Condensed GM Screen Button | &#9745;&#65039; |                       |
| Reset GM Screen Button     |    &#128306;    |                       |

### [Grid Scale Menu](https://www.foundryvtt-hub.com/package/scalegrid/) v1.2.1

This module adds an alternate way to setup grids by dragging out the grid sizes on the scene.

### [Handyfon's Dice Addiction](https://www.foundryvtt-hub.com/package/diceaddiction/) v1.1.0

Scratch your itch for digital dice.

- 34 new Textures
- 7 new Fonts to use with any set.
- "Golden Anvil" Dice Face for the D20

Dependencies: [Dice So Nice!](https://www.foundryvtt-hub.com/package/dice-so-nice/)

### [Health Estimate](https://www.foundryvtt-hub.com/package/healthestimate/) v28.0

A simple module for showing estimated health level of an enemy on mouseover, similar to Baldur's Gate games. Comes with a wide array of options to customize its behavior.

| Setting                      |                                 My Setting                                 | Comments |
| ---------------------------- | :------------------------------------------------------------------------: | :------: |
| **Style Settings**           |                                                                            |          |
| Use Color                    |                              &#9745;&#65039;                               |          |
| Smooth Gradient              |                              &#9745;&#65039;                               |          |
| Gradient Mode                |                        HSL (Red to Yellow to Green)                        |          |
| Color of Dead Tokens         |                                  Dark Red                                  |          |
| Outline                      |                                 Darken x 3                                 |          |
| Font Size                    |                                  X-Large                                   |          |
| Position                     |                                  Top x -1                                  |          |
| **Death Settings**           |                                                                            |          |
| Death State on Condition     |                              &#9745;&#65039;                               |          |
| Death State Name             |                                    Dead                                    |          |
| NPCs Die Immediately         |                              &#9745;&#65039;                               |          |
| Death Marker                 |                            icons/svg/skull.svg                             |          |
| **General Settings**         |                                                                            |          |
| Show Estimates: Users        |                                    All                                     |          |
| Show Estimates: Tokens       |                                    All                                     |          |
| Stages                       | Unconscious, Near Death, Badly Injured, Injured, Barely Injured, Uninjured |          |
| Perfectionism                |                              &#9745;&#65039;                               |          |
| Output Estimate to Chat      |                                 &#128306;                                  |          |
| Add Temporary Health         |                              &#9745;&#65039;                               |          |
| Hide on Tokens With 0 Max HP |                              &#9745;&#65039;                               |          |
| Use Vehicle Threshold        |                              &#9745;&#65039;                               |          |
| Threshold Stages             |                     Wrecked, Broken, Fully Functional                      |          |
| Vehicle Stages               |               Wrecked, Broken, Battered, Scratched, Pristine               |          |

### [Illandril's Hotbar Uses](https://www.foundryvtt-hub.com/package/illandril-hotbar-uses/) v2.4.2

Shows the number of uses left for item macros on the Hotbar that are created by dragging an inventory item, spell, or feat from a character sheet into the Hotbar.

| Setting             |   My Setting    | Comments |
| ------------------- | :-------------: | :------: |
| Show Maximum Values | &#9745;&#65039; |          |

### [Illandril's Inventory Sorter (5e)](https://www.foundryvtt-hub.com/package/illandril-inventory-sorter/) v1.1.4

Automatically sorts all actors' items (inventory, features, and spells) alphabetically (within each category).

### [Image Previewer](https://www.foundryvtt-hub.com/package/image-previewer/) v0.9

A little app to preview images when you hover over them in the file picker menu.

### [Item Macro](https://www.foundryvtt-hub.com/package/itemacro/) v1.5.7

It allow macros to be saved inside of an item and for various different ways to execute macors.

You can execute the macro from the "item" class using the executeMacro(...args) function, from the character sheet (if the settings are satisfied to do so), from the hotbar using the default rollItemMacro function for your system (if the settings are satisfied to do so), or from token-action-hud.

| Setting                               | My Setting | Comments |
| ------------------------------------- | :--------: | :------: |
| Debug (Client Setting)                | &#128306;  |          |
| Override Default Macro Execution      | &#128306;  |          |
| Character Sheet Hook                  | &#128306;  |          |
| Player Access                         | &#128306;  |          |
| Display Icon Only                     | &#128306;  |          |
| Right Click Override (Client Setting) | &#128306;  |          |

### [Item Piles](https://www.foundryvtt-hub.com/package/item-piles/) v1.4.8

Have you ever wished you could represent items in your scenes? A pile of items, something to interact with - or perhaps chests whose appearance changes depending on what happens to it, whether it's open, closed, full, or empty.

Then you need Item Piles!

In short, this module enables dropping items onto the canvas, which then get represented as a pile of items. In order to work in all systems without breaking or messing too much with the core functionality of Foundry, this creates an unlinked token & actor to hold these items. When a player double-clicks on an item pile token, it opens a custom UI to show what the pile contains and players can then take items from it.

Item Piles can also be configured to act as a container, where it can be open or closed, locked or unlocked, with the ability for the token that represents the pile to change image depending on its state. In addition, when an item pile is interacted with it can also play sounds for the action, such as opening, closing, or attempting to open a locked item pile. Sounds are only played for the user who attempted the action.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/libwrapper/), [Socketlib](https://www.foundryvtt-hub.com/package/socketlib/)

| Setting                         |             My Setting             |                                  Comments                                   |
| ------------------------------- | :--------------------------------: | :-------------------------------------------------------------------------: |
| **Currencies**                  |                                    | Only change here was to the names. (ex: data.currency.pp = Platinum Pieces) |
| **Item Filters**                |                                    |                                                                             |
| - type:spell,feat,class         |                                    |                                                                             |
| - data.weaponType:natural       |                                    |                                                                             |
| **Item Similarities**           |                                    |                                                                             |
| - name                          |                                    |                                                                             |
| - type                          |                                    |                                                                             |
| **General Settings**            |                                    |                                                                             |
| Actor Class Type                |             Character              |                                                                             |
| Item Quantity Attribute         |           data.quantity            |                                                                             |
| Output to Chat                  | Public - everyone can see messages |                                                                             |
| Auto-delete Empty Piles         |          &#9745;&#65039;           |                                                                             |
| Enable Trading                  |          &#9745;&#65039;           |                                                                             |
| Show Trade Button               |          &#9745;&#65039;           |                                                                             |
| Invert Ctrl + Double-click Open |             &#128306;              |                                                                             |
| Hide Actor Header Text          |          &#9745;&#65039;           |                                                                             |
| Preload Files                   |          &#9745;&#65039;           |                                                                             |
| Enable Debugging                |             &#128306;              |                                                                             |

### [JB2A - Patreon Complete Collection](https://www.foundryvtt-hub.com/package/jb2a_patreon/) :parking: v0.4.4

Awesome animations and effects collection. Used by several other modules to spice up the game. There is a free version of the module that provides limited animations.

| Setting                            |   My Setting    | Comments |
| ---------------------------------- | :-------------: | :------: |
| Info Chat Card - Disabled          | &#9745;&#65039; |          |
| JB2A - FXMaster Database Disabled  |    &#128306;    |          |
| JB2A - Location (Default: modules) |                 |          |

### [Jinker's Animated Art Pack](https://www.foundryvtt-hub.com/package/jaamod/) v0.7

Collection of Animated Art for use with VTT's in the top-down/overhead perspective. Files will be located in your "Data/modules/jaamod/AnimatedArt" folder.

### [Less Fog](https://www.foundryvtt-hub.com/package/lessfog/) v0.9.1

This module allows adjustments to the appearance of the FOW.

Dependencies: [Lib - Color Settings](https://www.foundryvtt-hub.com/package/colorsettings/)

| Setting               |   My Setting    | Comments |
| --------------------- | :-------------: | :------: |
| Global Daylight Color |    #eeeeeeff    |          |
| Global Darkness Color |    #484864ff    |          |
| Darkness - Dimming    |       0.5       |          |
| Darkness - Explored   |       0.4       |          |
| Darkness - Unexplored |       0.7       |          |
| Reveal Tokens         | &#9745;&#65039; |          |
| Reveal to All Players |    &#128306;    |          |

### [Levels](https://www.foundryvtt-hub.com/package/levels/):part_alternation_mark: v2.8.9.2

Create maps with multiple vertical levels

Dependencies: [Better Roofs](https://www.foundryvtt-hub.com/package/betterroofs/), [LibWrapper](https://www.foundryvtt-hub.com/package/lib-wrapper/), [Wall Height](https://www.foundryvtt-hub.com/package/wall-height/)

| Setting                               |   My Setting    | Comments |
| ------------------------------------- | :-------------: | :------: |
| Change Token Sized Based on Elevation |    &#128306;    |          |
| Elevation Scale Custom Multiplier     |        1        |          |
| Advanced Fog of War                   | &#9745;&#65039; |          |
| Reveal Token in Fog                   |    &#128306;    |          |
| Lock Elevation                        |    &#128306;    |          |
| Hide Elevation                        |   Don't Hide    |          |
| Enable Tooltips                       | &#9745;&#65039; |          |
| Precise Token Visibility              |    &#128306;    |          |
| Exact Token Visibility                |    &#128306;    |          |
| Fence                                 |    &#128306;    |          |
| Forec UI Refresh                      | &#9745;&#65039; |          |
| DEBUG: Enable Ray Visualization       |    &#128306;    |          |

### [LibBlockly](https://www.foundryvtt-hub.com/package/libblockly/) v0.6.0

An integration of Google Blockly into Foundry VirtualTop.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/lib-wrapper/)

| Setting                    |   My Setting    | Comments |
| -------------------------- | :-------------: | :------: |
| Allow Collapsed Blocks?    | &#9745;&#65039; |          |
| Allow Comments on Blocks?  | &#9745;&#65039; |          |
| Allow Disabled Blocks?     | &#9745;&#65039; |          |
| Show Trashcan?             | &#9745;&#65039; |          |
| Use Horizontal Layout?     |    &#128306;    |          |
| Show Zoom Controls?        | &#9745;&#65039; |          |
| Allow Mouse Wheel to Zoom? | &#9745;&#65039; |          |
| Zoom Start Scale           |        1        |          |
| Zoom Max Scale             |        3        |          |
| Zoom Min Scale             |       0.3       |          |
| Zoom Scale Speed           |       1.2       |          |
| Allow Pinch?               | &#9745;&#65039; |          |

### [Lib - Color Settings](https://www.foundryvtt-hub.com/packages/colorsettings) v2.8.4

Adds color picker as settings option and form option in Foundry VTT to use as library for module developers.

| Setting    |   My Setting    | Comments |
| ---------- | :-------------: | :------: |
| Show Error | &#9745;&#65039; |          |

### [Library: Chat Commands](https://www.foundryvtt-hub.com/package/_chatcommands/) v1.4.0

Allows for easy registration of custom chat commands such as /command

| Setting                                          |   My Setting    | Comments |
| ------------------------------------------------ | :-------------: | :------: |
| Should Commands Be Autocompleted                 | &#9745;&#65039; |          |
| Should Core Commands Be Included in Autocomplete | &#9745;&#65039; |          |

### [Library: DF Hotkeys](https://www.foundryvtt-hub.com/packages/lib-df-hotkeys/) v2.3.5

Library for Foundry VTT module developers to use. It allows modules to register their own Keyboard Shortcuts and gives way for users to then customize those hotkey bindings.

This module comes with a single hotkey pre-assigned for the Select Tool mapped to the S key.

| Setting              |                Default                | My Setting | Comments |
| -------------------- | :-----------------------------------: | :--------: | :------: |
| **Hotkeys Settings** |                                       |            |          |
| _General Hotkeys_    |                                       |            |          |
| Select Tool          | Key S - &#128306; &#128306; &#128306; |            |          |

### [Library: Scene Packer](https://www.foundryvtt-hub.com/package/scene-packer/) v2.5.3

It makes importing a Scene from a Compendium (via an "adventure module") work as though you build it in your world. You could even use this module as part of a "shared compendium" where you keep linked Journals for use in multiple worlds.

| Setting                          | My Setting |               Comments                |
| -------------------------------- | :--------: | :-----------------------------------: |
| Enable Scene Packer Context Menu | &#128306;  | &#9745;&#65039; When actively packing |
| Asset Timeout                    |    120     |                                       |

### [LibWrapper](https://www.foundryvtt-hub.com/packages/lib-wrapper) v1.12.5.0

Library for Foundry VTT which provides package developers with a simple way to modify core Foundry VTT code, while reducing the likelihood of conflict with other packages.

| Setting                      |   My Setting    |                    Comments                    |
| ---------------------------- | :-------------: | :--------------------------------------------: |
| **LibWrapper Settings Menu** |                 | Use this to prioritize or deprioritize modules |
| **General Settings**         |                 |                                                |
| Notify GM of Issues?         | &#9745;&#65039; |                                                |
| Notify Players of Issues     |    &#128306;    |                                                |
| High-Performance Mode        |    &#128306;    |                                                |
| Verbosity                    |      WARN       |                                                |

### [LiveKit AVClient](https://www.foundryvtt-hub.com/package/avclient-livekit/) v0.5.2

Use Live Kit to handle A/V.

| Setting                      |   My Setting    | Comments |
| ---------------------------- | :-------------: | :------: |
| Connection Quality Indicator | &#9745;&#65039; |          |
| Disable Receiving Audio      |    &#128306;    |          |
| Disable Receiving Video      |    &#128306;    |          |
| Use Separate Window for A/V  |    &#128306;    |          |
| Reset Meeting Room ID        |    &#128306;    |          |
| Enable Debug Logging         |    &#128306;    |          |

### [Lordu's Custom Dice for Dice So Nice](https://www.foundryvtt-hub.com/package/lordudice/) v0.34

Adds two custom dice systems and various new textures, colorsets and fonts for you to play with.

Dependencies: [Dice So Nice](https://www.foundryvtt-hub.com/package/dice-so-nice/)

### [Magic Items](https://www.foundryvtt-hub.com/package/magicitems/) v2.2.1

Adds the ability to create magical items with spells or feats that belong to the item itself, such as staffs or magic wands, which will be automatically inherited from the character who owns the item.

| Setting                        |   My Setting    | Comments |
| ------------------------------ | :-------------: | :------: |
| Only Identified                | &#9745;&#65039; |          |
| Hide Settings Tab From Players |    &#128306;    |          |

### [Mana's Compendium Importer](https://www.foundryvtt-hub.com/package/mkah-compendium-importer/) v1.1.0

Import & export compendiums.

### [Midi QOL](https://www.foundryvtt-hub.com/package/midi-qol/) :part_alternation_mark: v0.9.57

Attack/Damage/Saving throw automation.

Midi-qol goes through a standard sequence to complete the attack/spell cast. Midi-qol assumes that when you click the roll button you mean it. Midi-qol always applies damage/healing to the targeted tokens. If the item specifies a target of "self" midi-qol uses that instead (usually the trgeted token).

1. Consume any resources that should be consumed (ammunition, charges, spell slot etc).
2. If the item specifies a target template place the template and if &#9745;&#65039; select targets inside that template.
3. If the item has an attack roll the attack.
4. If auto check hits is &#9745;&#65039; modify the set of targets to be only those that were hit by the attack roll if there was one. If &#128306; the attack is assumed to hit all targets.
5. Roll the dmage if auto roll damage is &#9745;&#65039;.
6. Roll any saving throws for the affected targets (note there are options for players to roll their own saves). If &#128306; it is assumed that all targets failed their save.
7. Apply damage if &#9745;&#65039; taking into account damage resitance/immunities if &#9745;&#65039;. Midi-qol knows that damage can have different types so supports 10 pts of slahsing damage and 15 points of fire damage and applies immunities to each part seperately.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/lib-wrapper/), [Socketlib](https://www.foundryvtt-hub.com/package/socketlib/)

| Setting                                                                                                       |              My Setting              |                                             Comments                                              |
| ------------------------------------------------------------------------------------------------------------- | :----------------------------------: | :-----------------------------------------------------------------------------------------------: |
| **Workflow Settings**                                                                                         |                                      |                                                                                                   |
| **GM**                                                                                                        |                                      |                                                                                                   |
| Auto Roll Attack                                                                                              |           &#9745;&#65039;            |                                                                                                   |
| Skip Consume Resource Dialog                                                                                  |  Auto Consume Resources/Spell Slots  |                                                                                                   |
| Late Targeting                                                                                                |              &#128306;               |                                                                                                   |
| Auto Fast Forward Attack                                                                                      |           &#9745;&#65039;            |                                                                                                   |
| Auto Roll Damage                                                                                              |          Damage Roll Needed          |                                                                                                   |
| Auto Fast Forward Damage                                                                                      |           &#9745;&#65039;            |                                                                                                   |
| Remove Chat Card Buttons After Roll                                                                           |          Attack and Damage           |                                                                                                   |
| GM Roll Details Hiding                                                                                        |             Entire Roll              |                                                                                                   |
| Hide GM Attack/Damage/Saving Throw 3D Dice Rolls (Merge Card Only)                                            |              &#128306;               |                                                                                                   |
| For Attack/Damage Rolls Display with 'Dice So Nice Ghost Rolls' Rather Than Hiding the Roll (Merge Card Only) |           &#9745;&#65039;            |                                                                                                   |
| **Player**                                                                                                    |                                      |                                                                                                   |
| Auto Roll Attack                                                                                              |           &#9745;&#65039;            |                                                                                                   |
| Skip Consume Resource Dialog                                                                                  |  Auto Consume Resources/Spell Slots  |                                                                                                   |
| Auto Roll Damage                                                                                              |          Damage Roll Needed          |                                                                                                   |
| Auto Fast Forward Rolls                                                                                       |          Attack and Damage           |                                                                                                   |
| Remove Chat Card Buttons After Roll                                                                           |          Attack and Damage           |                                                                                                   |
| **Workflow**                                                                                                  |                                      |                                                                                                   |
| _Targeting_                                                                                                   |                                      |                                                                                                   |
| Auto Target on Template Draw                                                                                  |     Use DFTemplate Enhancements      |                                                                                                   |
| Auto Target for Ranged Target Spells/Attacks                                                                  |       Always - Ignore Defeated       |                                                                                                   |
| Require Targets to be Selected                                                                                |              In Combat               |                                                                                                   |
| _Specials_                                                                                                    |                                      |                                                                                                   |
| Add Actor on Use Macro to Sheet                                                                               |           &#9745;&#65039;            |                                                                                                   |
| Add Item on Use Macro to Sheet                                                                                |           &#9745;&#65039;            |                                                                                                   |
| Auto Apply Item Effects to Targets                                                                            |   Apply Effects Do Not Show Button   |                                                                                                   |
| Auto Apply Convenient Effects                                                                                 |      Apply CE and Auto Effects       |                                                                                                   |
| _Hits_                                                                                                        |                                      |                                                                                                   |
| Auto Check if Attack hits Target                                                                              |        Check - All See Result        |                                                                                                   |
| Display How Much an Attack Hit or Missed By (per Target)                                                      |              &#128306;               |                                                                                                   |
| _Saves_                                                                                                       |                                      |                                                                                                   |
| Auto Check Saves                                                                                              |      Save - All See Result Only      |                                                                                                   |
| Display Saving Throw DC                                                                                       |           &#9745;&#65039;            |                                                                                                   |
| Display if Save Had Advantage/Disadvantage                                                                    |           &#9745;&#65039;            |                                                                                                   |
| Search Item Description                                                                                       |              &#128306;               |                                                                                                   |
| Default Saving Throw Multiplier                                                                               |                 0.5                  |                                                                                                   |
| Prompt Players to Roll Saves                                                                                  |           Monk's Token Bar           |                                                                                                   |
| Prompt GM to Roll Saves (Unlinked)                                                                            |                 Auto                 |                                                                                                   |
| Prompt GM to Roll Saves (Linked)                                                                              |                 Auto                 |                                                                                                   |
| Delay Before Rolling For Players                                                                              |                  30                  |                                                                                                   |
| _Damage_                                                                                                      |                                      |                                                                                                   |
| Enhanced Damage Roll Dialog                                                                                   |              &#128306;               |                                                                                                   |
| Auto Apply Damage to Target                                                                                   |          Yes + Damage Card           |                                                                                                   |
| Show a Pleyers' Damage Card                                                                                   |              Don't Show              |                                                                                                   |
| Apply Damage Immunities                                                                                       |     Apply Immunities + Physical      |                                                                                                   |
| Require 'Magical' Property                                                                                    |  Off: Non-weapons Do Magical Damage  |                                                                                                   |
| Roll Other Formula if Present for rwak/mwak                                                                   |         Activation Condition         |                                                                                                   |
| Roll Other Formula for Spells                                                                                 |                 Off                  |                                                                                                   |
| Mark Wounded When HP Falls Below %                                                                            |                  50                  |                                                                                                   |
| When HP = 0: NPC Add Defeated, PC Add Convenient Effect Unconscious                                           |           &#9745;&#65039;            |                                                                                                   |
| _Concentration_                                                                                               |                                      |                                                                                                   |
| Enable Concentration Automation                                                                               |           &#9745;&#65039;            |                                                                                                   |
| Remove Concentration on Failed Save                                                                           |           &#9745;&#65039;            |                                                                                                   |
| Single Concentration Check                                                                                    |           &#9745;&#65039;            |                                                                                                   |
| Temp HP Concentration Check                                                                                   |              &#128306;               |                                                                                                   |
| **Misc**                                                                                                      |                                      | Use this tab using to import my settings from the file found in the Configuration Files directory |
| Show item Details in Chat Card                                                                                |                 None                 |                                                                                                   |
| Merge Rolls to One Card                                                                                       |            &#9745;&#65039            |                                                                                                   |
| Condense Attack/Damage Rolls                                                                                  |            &#9745;&#65039            |                                                                                                   |
| Chat Cards Use Token Name                                                                                     |              &#128306;               |                                                                                                   |
| Move Roll Formula to Tooltip                                                                                  | Formula -> Tooltip + Adv Attribution |                                                                                                   |
| Chat Cards Use Token Name                                                                                     |              &#128306;               |                                                                                                   |
| Use Actor Portrait in Chat Cards                                                                              |              &#128306;               |                                                                                                   |
| Keep Roll Statistics                                                                                          |           &#9745;&#65039;            |                                                                                                   |
| Save Statistics Every                                                                                         |                  20                  |                                                                                                   |
| Player Statstics Only                                                                                         |              &#128306;               |                                                                                                   |
| Fix Sticky Adv/Ctrl/Shift                                                                                     |           &#9745;&#65039;            |                                                                                                   |
| Enable Midi-QOL Custom Sounds                                                                                 |           &#9745;&#65039;            |                                                                                                   |
| **Optional**                                                                                                  |                                      |                                                                                                   |
| Check Player Reactions                                                                                        |                  ON                  |                                                                                                   |
| Check NPC/GM Reactions                                                                                        |                  ON                  |                                                                                                   |
| Reaction Check Timeout                                                                                        |                  30                  |                                                                                                   |
| Show Chat Prompt For Reactions                                                                                |              &#128306;               |                                                                                                   |
| Show Attack Roll                                                                                              |          Attack Roll Total           |                                                                                                   |
| Enforce Reactions                                                                                             |      All Actors: Prompt if Used      |                                                                                                   |
| Enforce Bonus Actions                                                                                         |      All Actors: Prompt if Used      |                                                                                                   |
| Record Attacks of Opportunity                                                                                 |              All Actors              |                                                                                                   |
| (Experimental) Optional Game Rules Work With Better Rolls , But Triggers a LibWrapper Warning                 |           &#9745;&#65039;            |                                                                                                   |
| Incapacitated Actors Can't Take Actions or Reactions                                                          |           &#9745;&#65039;            |                                                                                                   |
| Check Flanking                                                                                                |       CE Flanking + Advantage        |                                                                                                   |
| Hidden/Invisible Attackers Have Advantage on Attacks (Requires CUB or CV)                                     |           &#9745;&#65039;            |                                                                                                   |
| Attacking Removes Hidden/Invisible (Requires Conditional Visibility or CUB)                                   |           &#9745;&#65039;            |                                                                                                   |
| Walls Block Ranged Attacks                                                                                    |          DnD5e Helpers + AC          |                                                                                                   |
| Include Height Differences in Distance Calcs                                                                  |              &#128306;               |                                                                                                   |
| Ranged Attacks When a Foe is CloserTthan This Have Disadvantage. 0 Disables                                   |                  5                   |                                                                                                   |
| (House Rule) Enabling This Means That Only The Most Effective Damage Reduction Will Apply.                    |              &#128306;               |                                                                                                   |
| (House Rule) Critical/Fumble Always Succeed/Fail For Saving Throws                                            |              &#128306;               |                                                                                                   |
| (House Rule) Critical Roll Margin                                                                             |                  -1                  |                                                                                                   |
| (House Rule) Ranged Attacks at Foes Near Allies Have Disadvantage                                             |                  4                   |                                                                                                   |
| Active Defence                                                                                                |              &#128306;               |                                                                                                   |
| Enable Challenge Mode Armor Class                                                                             |              &#128306;               |                                                                                                   |
| Enable House Version Challenge Mode Armor Class                                                               |              &#128306;               |                                                                                                   |
| Enable ddb-game-log support                                                                                   |           &#9745;&#65039;            |                                                                                                   |
| **General Settings**                                                                                          |                                      |                                                                                                   |
| Enable Roll Automation Support                                                                                |           &#9745;&#65039;            |                                                                                                   |
| Late Targeting                                                                                                |              &#128306;               |                                                                                                   |
| Add Attack/Damage Buttons to Item Inventory List                                                              |           &#9745;&#65039;            |                                                                                                   |
| Item Delete Check                                                                                             |           &#9745;&#65039;            |                                                                                                   |
| GM Sees All Whispered Messages                                                                                |              &#128306;               |                                                                                                   |
| Really Hide Private/Blind/Self Rolls                                                                          |           &#9745;&#65039;            |                                                                                                   |
| Fast Forward Ability Rolls                                                                                    |           &#9745;&#65039;            |                                                                                                   |
| Drag and Drop Targeting                                                                                       |           &#9745;&#65039;            |                                                                                                   |
| Choose How to Roll Critical Damage                                                                            |          Max Critical Dice           |                                                                                                   |
| Add Damage Buttons to Chat Message                                                                            |                 None                 |                                                                                                   |
| Colored Border Messages                                                                                       |          Border + Name Text          |                                                                                                   |
| Untarget at End of Turn                                                                                       |       Untarget Dead After Roll       |                                                                                                   |
| Players Control Owned Hidden Tokens                                                                           |           &#9745;&#65039;            |                                                                                                   |
| Enable Debug                                                                                                  |                 None                 |                                                                                                   |
| Log Execution Timing                                                                                          |              &#128306;               |                                                                                                   |

### [Midi SRD](https://www.foundryvtt-hub.com/package/midi-srd/) v0.2.03

The partner pack to the DAE SRD: A compendium pack for SRD items, feats etc configured for use with Dynamic Active Effects as well as Midi QOL.

Dependencies: [Advanced Macros](https://www.foundryvtt-hub.com/package/advanced-macros/), [Dynamic Effects with Active Effects](https://www.foundryvtt-hub.com/package/dae/), [Midi QOL](https://www.foundryvtt-hub.com/package/midi-qol/), [Simple Calendar](https://www.foundryvtt-hub.com/package/foundryvtt-simple-calendar/), [Times Up](https://www.foundryvtt-hub.com/package/times-up/)

### [Minimal UI](https://www.foundryvtt-hub.com/package/minimal-ui/) :part_alternation_mark: v1.4.24

Extremely Configurable UI module, allows the user to hide, collapse or auto-hide components separately.

This includes hiding Foundry's Logo, Players List, Scene Navigation and Macro Bar.

Dependencies: [Lib - Color Settings](https://www.foundryvtt-hub.com/package/colorsettings/)

| Setting                              |              My Setting              |                  Comments                  |
| ------------------------------------ | :----------------------------------: | :----------------------------------------: |
| Border Colors                        |              #0091ff80               |                                            |
| Shadow Colors                        |              #006cff80               |                                            |
| Shadow strength                      |                  5                   |                                            |
| Hover Opacity Percentage             |                 100                  |                                            |
| Foundry Logo Visibility              |               Standard               |                                            |
| Foundry Logo Behavior                | Logo Toggles Hide All UI Except Chat |                                            |
| Foundry Logo Image                   |            icons/fvtt.png            | This will be overridden by My Tab settings |
| Scene Navigation Style               |            Start Visible             |                                            |
| Scene Navigation Size                |               Standard               |                                            |
| Left Controls Size                   |                Small                 |                                            |
| Left Controls Sub-Menu Style         |            Always Visible            |                                            |
| Macro Hotbar                         |            Always Visible            |                                            |
| Macro Hotbar Position                |        Left (Foundry Default)        |                                            |
| Macro Hotbar Manual Pixel Position   |                 400                  |                                            |
| Ride Side Panel Behavior             |            Start Visible             |                                            |
| Player List Behavior                 |            Always Visible            |                                            |
| Player List Size                     |               Standard               |                                            |
| Ping Logger with Minimal UI Behavior |                 Show                 |                                            |
| No Camera Behavior                   |               Reduced                |                                            |

### [Module Management+](https://www.foundryvtt-hub.com/package/module-credits/) v1.1.11

Is packed with features and design improvements designed to make managing large module list easy. MM+ provides more detailed information about modules at a glance and allows vou to read packages changelogs/readmes directly in foundry meant to help you find answers quickly. Some of the notable features of Module Management+ is the Addition of new tags such as readme, changelog, issues and authors. A cleaner easier list view of the modules with stripped rows and more spacing to make the viewing experiance easier on the eyes. The ability to see conflicts and known issues with your installed modules.

| Setting                     |   My Setting    | Comments |
| --------------------------- | :-------------: | :------: |
| Show New Changelogs on Load |    &#128306;    |          |
| Enable Global Conflicts     | &#9745;&#65039; |          |
| Big Picture Mode            | #9745;&#65039;  |          |

### [Monk's Active Tile Triggers](https://www.foundryvtt-hub.com/package/monks-active-tiles/) :part_alternation_mark: v1.0.79

Want to teleport, or open doors, or hide characters, or display a message, or play a sound, or change a token's elevation when a token walks over a tile... now you can.

| Setting                  |   My Setting    | Comments |
| ------------------------ | :-------------: | :------: |
| Default Trigger          |    On Enter     |          |
| Use Core Macro Execute   |    &#128306;    |          |
| Allow Players            |    &#128306;    |          |
| Drop Item on Canvas      | &#9745;&#65039; |          |
| Door Triggers            | &#9745;&#65039; |          |
| Show Teleport Wash       | &#9745;&#65039; |          |
| Teleport Wash Color      |     #c0c0c0     |          |
| Show Hints               | &#9745;&#65039; |          |
| Prevent When Paused      | &#9745;&#65039; |          |
| Allow Door Click-Through | &#9745;&#65039; |          |

### [Monk's Enhanced Journal](https://www.foundryvtt-hub.com/package/monks-enhanced-journal/) v1.0.60

Allows for multiple types of journal entries, and adds increased functionality such as searching, private notes, bookmarks, and tabs.

| Setting                    |    My Setting    |               Comments               |
| -------------------------- | :--------------: | :----------------------------------: |
| **Currency**               |                  |         I left the defaults          |
| **Person Attributes**      |                  |         I left the defaults          |
| **Place Attributes**       |                  |         I left the defaults          |
| **General Settings**       |                  |                                      |
| Allow Players              | &#9745;&#65039;  |                                      |
| Open in New Tab            |    &#128306;     |                                      |
| Dice Rolling Module        | Monk's TokenBar  |                                      |
| Use Quest Objectives       | &#9745;&#65039;  |                                      |
| Display Quests             | &#9745;&#65039;  |                                      |
| Default Rune State         | &#9745;&#65039;  |                                      |
| Show Player Permissions    | &#9745;&#65039;  |                                      |
| Note HUD Permission        | &#9745;&#65039;  |                                      |
| Start Collapsed            |    &#128306;     |                                      |
| Open Links                 |    &#128306;     |                                      |
| Show Folder Sorting        |    &#128306;     |                                      |
| Show Zero Quantity         |    &#128306;     |                                      |
| Inactive Players Loot      | &#9745;&#65039;  |                                      |
| Show Chat Message          | &#9745;&#65039;  |                                      |
| Hide Inline Links          | &#9745;&#65039;  |                                      |
| Hide Compendium Rolltables |    &#128306;     |                                      |
| Show Bookmark Bar          | &#9745;&#65039;  |                                      |
| Show Editor Menu           |    &#128306;     |                                      |
| Distribution Conversion    | &#9745;&#65039;  |                                      |
| Purchase Conversion        | &#9745;&#65039;  |                                      |
| Loot Sheet                 |    Item Piles    |                                      |
| Loot Entity                | -- Create New -- |                                      |
| Loot Folder                |                  | Choose an actor folder in your world |

### [Monk's Hotbar Expansion](https://www.foundryvtt-hub.com/package/monks-hotbar-expansion/) v1.0.14

This is an expansion of the current Hotbar so that a user can see all rows of the Hotbar at once and edit all of the macros at once. Including the option to clear an entire row.

| Setting            |   My Setting    | Comments |
| ------------------ | :-------------: | :------: |
| Number of Rows     |        5        |          |
| Reverse            | &#9745;&#65039; |          |
| Hide the First Row |    &#128306;    |          |
| Start Collapsed    | &#9745;&#65039; |          |
| Collapse on Select | &#9745;&#65039; |          |
| Hide Page Arrows   |    &#128306;    |          |

### [Monk's Little Details](https://www.foundryvtt-hub.com/package/monks-little-details/) v1.0.52

Change the look and feel of a lot of little things within Foundry. And added a handful of things to help with Automation.

- Turn Notifications. Just before a player's turn and at the beginning of the player's turn, a notification will show as well as an accompanying sound to remind them.
- Combat Encounter Automation. When a combat is started, the Encounter Dialog will popout and will switch to the Chat Tab. I find that I'm always doing this, so might as well automate this.
- The Encounter Dialog will also close once the encounter is complete and there are no other active encounters.
- Option of automatically hiding enemy tokens on the list while building an encounter.
- Automatically sets the defeated status when an NPC token reaches 0 HP.
- Reveal initiative when changing a token from invisible to visible.
- Removed selected targets if you are the GM.
- Show Combat CR. When creating a new encounter, display the calculated CR. Not 100% accurate but it should give the GM an idea if the encounter needs to be scaled.

| Setting                                          |                                              My Setting                                               | Comments |
| ------------------------------------------------ | :---------------------------------------------------------------------------------------------------: | :------: |
| **System Changes**                               |                                                                                                       |          |
| Swap Target and Settings Button on the Token HUD |                                            &#9745;&#65039;                                            |          |
| Alter the Token HUD Status Effects               |                                            &#9745;&#65039;                                            |          |
| Sort Statuses                                    |                                                 Rows                                                  |          |
| Highlight Token HUD Status Effects               |                                            &#9745;&#65039;                                            |          |
| Add DnD Statuses                                 |                                            &#9745;&#65039;                                            |          |
| Use DnD5e Invisible Icon                         |                                            &#9745;&#65039;                                            |          |
| Change Core CSS                                  |                                            &#9745;&#65039;                                            |          |
| Compendium Scene View Artwork                    |                                            &#9745;&#65039;                                            |          |
| **Combat Tracker**                               |                                                                                                       |          |
| Show Encounter CR                                |                                            &#9745;&#65039;                                            |          |
| Switch to Comabt Tab on Combat Start             |                                            &#9745;&#65039;                                            |          |
| Hide Enemies                                     |                                               &#128306;                                               |          |
| Popout Combat Dialog                             |                                       Do Not Open Combat Window                                       |          |
| Combat Dialog Position                           |                                                  ---                                                  |          |
| Close Combat When Done                           |                                            &#9745;&#65039;                                            |          |
| Prevent Token Combat Removal                     |                                            &#9745;&#65039;                                            |          |
| Automatically Set Defeated                       |                                              NPC Zero HP                                              |          |
| Turn Dead Invisible                              |                                               &#128306;                                               |          |
| Automatically Reveal Combat Tracker Character    |                                            &#9745;&#65039;                                            |          |
| Add Combat Bars                                  |                                               &#128306;                                               |          |
| Auto Scroll Combat Tracker                       |                                            &#9745;&#65039;                                            |          |
| **Conbat Turn**                                  |                                                                                                       |          |
| Show 'Next Up' Notification                      |                                            &#9745;&#65039;                                            |          |
| Show 'Your Turn' Notification                    |                                            &#9745;&#65039;                                            |          |
| Next Turn Sound                                  |                                            &#9745;&#65039;                                            |          |
| Next Turn Sound Path                             |                             modules/monks-little-details/sounds/next.ogg                              |          |
| Current Turn Sound                               |                                            &#9745;&#65039;                                            |          |
| Current Turn Sound Path                          |                             modules/monks-little-details/sounds/turn.ogg                              |          |
| New Round Sound                                  |                                            &#9745;&#65039;                                            |          |
| New Round Sound Path                             |                             modules/monks-little-details/sounds/round.ogg                             |          |
| Volume                                           |                                                  70                                                   |          |
| Clear Targets After Turn                         |                                            &#9745;&#65039;                                            |          |
| Remember Previous                                |                                            &#9745;&#65039;                                            |          |
| Combat Round Messages                            |                                            &#9745;&#65039;                                            |          |
| Show Start Token                                 |                                            &#9745;&#65039;                                            |          |
| Pan to Combatant                                 |                                            &#9745;&#65039;                                            |          |
| **Combat Token Highlight**                       |                                                                                                       |          |
| Show Token Highlight                             |                                            &#9745;&#65039;                                            |          |
| Clear Token Highlight                            |                                            &#9745;&#65039;                                            |          |
| Combat Highlight Animation Speed                 |                                                  100                                                  |          |
| Combat Highlight Image                           |                           modules/monks-little-details/markers/marker07.png                           |          |
| Hostile Highlight Image                          |                           modules/monks-little-details/markers/marker01.png                           |          |
| Highlight Size                                   |                                                  1.5                                                  |          |
| Combat Highlight Animation                       |                                                 Pulse                                                 |          |
| Hostile Highlight Animation                      |                                               Clockwise                                               |          |
| Select Combatants                                |                                            &#9745;&#65039;                                            |          |
| **Added Features**                               |                                                                                                       |          |
| Actor Sound Effects                              |                                                 NPCs                                                  |          |
| Show Scene Palette                               |                                            &#9745;&#65039;                                            |          |
| Find My Token                                    |                                            &#9745;&#65039;                                            |          |
| Retain Notify                                    |                                            &#9745;&#65039;                                            |          |
| Move the Pause Icon Up                           |                                               &#128306;                                               |          |
| Resposition Collapse Button                      |                                            &#9745;&#65039;                                            |          |
| Show Bloodsplat                                  |                                            No Bloodsplats                                             |          |
| Bloodsplat Color                                 |                                                #ff0000                                                |          |
| Module Management Changes                        |                                            &#9745;&#65039;                                            |          |
| Macro Tabs                                       |                                            &#9745;&#65039;                                            |          |
| Loot Image                                       | "https://assets.forge-vtt.com/60e10c9b7646f25566063a74/custom/icons/items/Treasure%20Chest%203B.webp" |          |
| Loot Image Size                                  |                                                  0.9                                                  |          |
| Use Keys to Swap Tools                           |                                               &#128306;                                               |          |

### [Monk's Player Settings](https://www.foundryvtt-hub.com/package/monks-player-settings/) v1.0.3

Allow the GM to change player settings and sync client stings between browsers.

| Setting       |   My Setting    | Comments |
| ------------- | :-------------: | :------: |
| Sync Settings | &#9745;&#65039; |          |

### [Monk's Scene Navigation](https://www.foundryvtt-hub.com/package/monks-scene-navigation/) v1.0.23

An update to the standard Scene Navigation interface Instead of expanding across the page, the scenes are now limited to one line and the folder structure of the Scene's Directory is preserved allowing you to reorganise your scenes based on Chapters in the game and have less space taken up on your screen.

| Setting                  |   My Setting    | Comments |
| ------------------------ | :-------------: | :------: |
| Click to View            | &#9745;&#65039; |          |
| Scene Icons              | &#9745;&#65039; |          |
| Modify Scene Bar         | &#9745;&#65039; |          |
| Scene Folders Position   |      Front      |          |
| Show Folders for Players | &#9745;&#65039; |          |
| Display Real Name        | &#9745;&#65039; |          |
| Display Background       | &#9745;&#65039; |          |
| Quick Navigation Toggle  |    &#128306;    |          |
| Double-Click Activate    | &#9745;&#65039; |          |
| Minimize on Activate     |    &#128306;    |          |
| Minimize During Combat   | &#9745;&#65039; |          |

### [Monk's Sound Enhancements](https://www.foundryvtt-hub.com/package/monks-sound-enhancements/) v1.0.2

A module to enhance the way Foundry handles playlists and sounds.

## [Monk's TokenBar](https://www.foundryvtt-hub.com/package/monks-tokenbar/) v1.0.67

Player tokens on the current scene are automatically added and removed from this bar. It shows you the current AC and Passive Perception and clicking on the image will center the screen on that token, in case you lose track of where the token is.

- Limit Movement. You can limit the players token movement using the buttons on the left side. Free Movement, No Movement, and Combat Movement are availabale as well as right-clicking on a TokenBar token will let you set the movement restrictions specifically for that token.
- Saving Throw Dialog. Click the saving throw button to open a dialog that will request a saving throw from each player added to the list. Optionally set a DC to beat and it will automatically calculate which player succeeded.
- Contested Roll Dialog. Click the contested roll button to open a dialog that will request a contested roll between two tokens. Once rolled it will calculate and display who won.
- Assign XP Dialog. Click the assign XP button to open a dialog that will assign XP to the current players ont he map. Additionally when an encounter is completed it will open a dialog with the XP for the encounter calculated and divided evenly among the players that were involved.
- Lootables. Convert NPC's to lootable. TokenBar will remove all feats of the character that should not be lootable and will save them so that the character can be reverted back from lootable.

| Setting                            |                                     My Setting                                      | Comments |
| ---------------------------------- | :---------------------------------------------------------------------------------: | :------: |
| **Edit Stats**                     |                                                                                     |          |
| 1st                                |                            Shield - attributes.ac.value                             |          |
| 2nd                                |                              Eye - skills.prc.passive                               |          |
| **Tokenbar Settings**              |                                                                                     |          |
| Allow Player to Use                |                                   &#9745;&#65039;                                   |          |
| Disable Tokenbar                   |                                      &#128306;                                      |          |
| Show Vertical                      |                                      &#128306;                                      |          |
| Double-click Action                |                                Open Character Sheet                                 |          |
| **Icon Settings**                  |                                                                                     |          |
| Size of Tokens                     |                                         50                                          |          |
| Token Resolution                   |                                         100                                         |          |
| Show Resource Bars                 |                                   &#9745;&#65039;                                   |          |
| Token Pictures                     |                                     Token Image                                     |          |
| Show Inspiration                   |                                   &#9745;&#65039;                                   |          |
| Show Disable Panning Option        |                                      &#128306;                                      |          |
| **Movement Settings**              |                                                                                     |          |
| Notify on Movement Change          |                                   &#9745;&#65039;                                   |          |
| Change Movement to Combat          |                                   &#9745;&#65039;                                   |          |
| Allow Active Combatant NPC to Move |                                   &#9745;&#65039;                                   |          |
| Allow Movement After Turn          |                                      &#128306;                                      |          |
| Set Movement After Combat          |                                    Free Movement                                    |          |
| Show on Tracker                    |                                   &#9745;&#65039;                                   |          |
| **After Combat Settings**          |                                                                                     |          |
| Whisper Player on Level Up         |                                   &#9745;&#65039;                                   |          |
| Show XP Dialog                     |                                   &#9745;&#65039;                                   |          |
| How to Divide XP Among Players     |                               Divide XP Among Players                               |          |
| Gold Formula                       | Math.round(0.6 \* 10 \* (10 \*\* (0.15 \* ({{ actor.data.data.details.cr}} ?? 0)))) |          |
| How to Distribute                  |               Transfer Items to a Single Entity and Add Link on Scene               |          |
| Distribute Enemies Loot            |                                     Item Piles                                      |          |
| Loot Entity                        |                                  -- Create New --                                   |          |
| Loot Folder                        |                                                                                     |          |
| Open the Loot Entity               |                                       No One                                        |          |
| Show Lootable Menu                 |                                   &#9745;&#65039;                                   |          |
| **Request Roll Settings**          |                                                                                     |          |
| Request Roll Sound                 |                 modules/monks-tokenbar/sounds/RollRequestAlert.ogg                  |          |
| Delete Message After Grab          |                                   &#9745;&#65039;                                   |          |
| Capture Saving Throw               |                                      &#128306;                                      |          |

### [Monk's Wall Enhancement](https://www.foundryvtt-hub.com/package/monks-wall-enhancement/) v1.0.9

Improvements to the Core Wall functionality, by adding the option to drag points together, double-click to split a wall, join points together, and free hand drawing of a wall

| Setting                   |   My Setting    | Comments |
| ------------------------- | :-------------: | :------: |
| Show Drag Points Together | &#9745;&#65039; |          |
| Allow Double-Click Split  | &#9745;&#65039; |          |
| Condense Wall Types       |    &#128306;    |          |
| Freehand Tolerance        |       25        |          |
| Toggle Secret Door        |    &#128306;    |          |

### [Moulinette Core](https://www.foundryvtt-hub.com/package/moulinette-core/) :part_alternation_mark: :parking: v4.7.0

Provides multiple tools for DMs to help them prepare their games. The core module is required for all other Moulinette sub-modules but doesn't provide any feature by itself. Make sure to install at least 1 other Moulinette module from the list below.

| Setting                 |   My Setting    | Comments |
| ----------------------- | :-------------: | :------: |
| Assets Custom Path      |     /custom     |          |
| Debug Indexing Process  |    &#128306;    |          |
| Showcase Content        | &#9745;&#65039; |          |
| Enable Moulinette Cloud | &#9745;&#65039; |          |
| Moulinette Cloud Color  |     Default     |          |
| S3 Bucket               |                 |          |
| Browse Mode             |   By Creator    |          |
| Moulinette FilePicker   |    &#128306;    |          |
| Interface               |     Default     |          |
| Moulinette Sub-Controls | &#9745;&#65039; |          |

- [Moulinette Game Icons (module)](https://www.foundryvtt-hub.com/package/moulinette-gameicons/) v3.3.0

  This module for Moulinette adds capabilities for searching and downloading icons from game-icons.net. Search and download desired icons. Specify the background and foreground colors you'd like to use.

- [Moulinette Image Search (module)](https://www.foundryvtt-hub.com/package/moulinette-imagesearch/) v3.1.1

  This module for Moulinette adds capabilities for searching images and generating articles, tiles or tokens on the fly.

  | Setting             | My Setting | Comments |
  | ------------------- | :--------: | :------: |
  | Bing Search API Key |   {key}    |          |

- [Moulinette Scenes (module)](https://www.foundryvtt-hub.com/package/moulinette-scenes/) v3.3.2

  This module for Moulinette adds capabilities for downloading scenes submitted by the community.

- [Moulinette Sounds & SoundPad (module)](https://www.foundryvtt-hub.com/package/moulinette-sounds/) v3.7.0

  This module for Moulinette adds capabilities for managing your sounds and create a soundpad. Bring your own sounds, manage them and assign them to a soundpad as you like.

  | Setting                       |   My Setting    | Comments |
  | ----------------------------- | :-------------: | :------: |
  | Soundboard: Shortcuts Per Row |       10        |          |
  | Soundboard: Number of Rows    |        1        |          |
  | Default Effect Radius         |       10        |          |
  | Repeat Sounds By Default      |    &#128306;    |          |
  | Hide UI below Soundboard      |    &#128306;    |          |
  | Hide TTA Warning              |    &#128306;    |          |
  | SoundPad: Download Sounds     | &#9745;&#65039; |          |

- [Moulinette Tiles (module)](https://www.foundryvtt-hub.com/package/moulinette-tiles/) v4.9.0

  This module for Moulinette adds capabilities for downloading tiles and adding them to your scenes. You can also bring your own images and manage them. Drag & drop images to your scenes to add tiles, actors' tokens or journal notes

  | Setting              |         My Setting          | Comments |
  | -------------------- | :-------------------------: | :------: |
  | Show Video Thumbnail |       &#9745;&#65039;       |          |
  | Macros for Tiles     | Baileywiki Maps Town Macros |          |

### [Multiclass Spellbook Filter](https://www.foundryvtt-hub.com/package/spell-class-filter-for-5e/) v0.3

Adds options to organize spells by class. Useful for multiclass characters.

| Setting                                          |   My Setting    | Comments |
| ------------------------------------------------ | :-------------: | :------: |
| Client Setting: Enable FilterSelector on Actor   | &#9745;&#65039; |          |
| Client Setting: Cover Spell Icon With Class Icon |    &#128306;    |          |

### [MyTab](https://www.foundryvtt-hub.com/package/mytab/) v0.4

Change the title, favicon, anvil icon, pause icon and text, and mouse cursor of your browser tab.

| Setting                      |        My Setting         | Comments |
| ---------------------------- | :-----------------------: | :------: |
| Page-Title                   |      {Campaign Name}      |          |
| Active Scene as Page-Title   |        After-Title        |          |
| Favicon                      |     {Custom Favicon}      |          |
| Anvil Icon                   |    {Custom Anvil Icon}    |          |
| Display Button OpenPauseMenu |         &#128306;         |          |
| Standard Pause Text          |          Paused           |          |
| Pause Almost Over SFX        |     sounds/notify.wav     |          |
| Pause Icon                   |    {Custom Pause Icon}    |          |
| Time Presets                 |        300,600,900        |          |
| Pause Background             |       Behind Stripe       |          |
| Pause Location               |          Center           |          |
| Pause Scale                  |        50% Bigger         |          |
| Pause Icon Animation         |          Scaling          |          |
| Pause Icon Animation Speed   |           Slow            |          |
| Pause Background Image       | {Custom Background Image} |          |
| Pick a Cursor                |            leg            |          |

### [Nice More Dice](https://www.foundryvtt-hub.com/package/nice-more-dice/) v1.1.0

Adds more textures and dice faces to give users more customization options for their dice.

Dependencies: [Dice So Nice](https://www.foundryvtt-hub.com/package/dice-so-nice/)

### [Parallaxia](https://www.foundryvtt-hub.com/package/parallaxia/) v0.4.0e

Extend base Tile functionality to allow custom dynamic visuals. For example create the effect of movement of map elements like drifting clouds by rolling a wrapping texture. Have a shadow fly over the map. Create the illusion of depth by the name-lending parallax of layered tiles, etc.

### [Party Overview](https://www.foundryvtt-hub.com/package/party-overview/) v2.12.6

A quick overview about the players that have tokens placed on the currently active scene. At a glance, you will see

- Vitals (hit points, wounds)
- Inspiration, Hero Points, Bennies, Advantage.
- Defenses (Armor Class, Saving Throws)
- All languages your players might know and which one of them can speak said language.
- Using filters, you can hide and show players to your hearts desire, to quickly get the information you need in the heat of the battle.

| Setting                               |              My Setting              | Comments |
| ------------------------------------- | :----------------------------------: | :------: |
| **Party Overview System Settings**    |                                      |          |
| System Provider                       | Generic DnD5e - Fifth Edition System |          |
| Languges                              |           &#9745;&#65039;            |          |
| Wealth                                |           &#9745;&#65039;            |          |
| Background                            |           &#9745;&#65039;            |          |
| Saving Throws                         |           &#9745;&#65039;            |          |
| Proficiencies                         |           &#9745;&#65039;            |          |
| Tools                                 |           &#9745;&#65039;            |          |
| **General Settings**                  |                                      |          |
| Grant Players Access to the Overview? |           &#9745;&#65039;            |          |

### [Perfect Vision](https://www.foundryvtt-hub.com/package/perfect-vision/):part_alternation_mark: v3.9.11

Darkvision rules as well as other vision-related features and improvements.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/lib-wrapper/)

| Setting                    |       My Setting       | Comments |
| -------------------------- | :--------------------: | :------: |
| Vision Rules               | Dungeons & Dragons 5e  |          |
| Dim Vision in Darkness     | Dim Light (Monochrome) | Disabled |
| Dim Vision in Dim Light    |      Bright Light      | Disabled |
| Bright Vision in Darkness  |      Bright Light      | Disabled |
| Bright Vision in Dim Light |      Bright Light      | Disabled |
| Monochrome Vision Color    |        #ffffff         |          |

### [Permission Viewer](https://www.foundryvtt-hub.com/package/permission_viewer/) v0.9.3

Quickly see which entities (Journal entries, Actors, etc..) are visible to all players or visible to a specific player. It also adds a quick share button in the sidebars and when showing journal entries to players, will ask if you want to share them, show them to all or show them only to players who have permission to view it.

| Setting                   | My Setting | Comments |
| ------------------------- | :--------: | :------: |
| Limited Permission Dialog | &#128306;  |          |

### [Pin Cushion](https://www.foundryvtt-hub.com/package/pin-cushion/) v1.5.0

- Changes the dropdown of map pin icons into a filepicker so users can select any icons they like
- Adds the ability to double-click the canvas while on the Notes Layer and create a map pin (and corresponding Journal Entry)
- Adds a preview of the associated Journal Entry when you hover over a map pin

| Setting                          | My Setting | Comments |
| -------------------------------- | :--------: | :------: |
| Show Journal Preview             | &#128306;  |          |
| Journal Preview Type             |    HTML    |          |
| Preview Maximum Length           |    500     |          |
| Preview Delay                    |    500     |          |
| Default Journal Entry Permission |    None    |          |
| Default Journal Entry Folder     |    None    |          |

### [Ping Logger](https://www.foundryvtt-hub.com/package/ping-logger/) v1.2.15

A very tiny module that displays players' latency next to their name.

| Setting                  | My Setting | Comments |
| ------------------------ | :--------: | :------: |
| Ping Interval in Seconds |     30     |          |

### [Pings](https://www.foundryvtt-hub.com/package/pings/) :part_alternation_mark: v1.3.0

Adds the ability to ping on the map to highlight points of interest. Default is Left Click to ping, Shift + Left Click to move everyone's screen to your ping.

| Setting                               |   My Setting    | Comments |
| ------------------------------------- | :-------------: | :------: |
| Minimum Move Permission               |   GAMEMASTER    |          |
| Mouse Button to Press for a Ping      |    LeftClick    |          |
| Mouse Button for a Screen-Moving Ping | Shift+LeftClick |          |
| Mouse Button Press Duration           |       500       |          |
| Key to Press for a Ping               |                 |          |
| Key for a Sreen-Moving Ping           |                 |          |
| Show Player Name                      | &#9745;&#65039; |          |
| Ping Image                            |                 |          |
| Scale                                 |        1        |          |
| Duration                              |        5        |          |
| Rotation                              | &#9745;&#65039; |          |
| Rotation Speed                        |        6        |          |
| Size Change                           | &#9745;&#65039; |          |
| Size Change Amount                    |      0.125      |          |
| Size Change Speed                     |        3        |          |

### [Polyglot](https://www.foundryvtt-hub.com/package/polyglot/) v1.7.39

Talk to others using a language you can speak and scrambles text you can't understand, into several fantasy scripts.

Chat
A language selection dropbox is available above the chat text box so you can select which language you want your character to speak in.
When loading a world, only the last 100 messages are scrambled to avoid making load times insane on a big chat log.

Custom Languages
You can add your own languages to Polyglot by inputting them into a module setting.

Custom Fonts
You can add your own fonts to Polyglot, refer to the module's wiki for more information.

| Setting                       |   My Setting    |                  Comments                   |
| ----------------------------- | :-------------: | :-----------------------------------------: |
| **Font Settings**             |                 | This should auto-fill based on custom fonts |
| **Language Settings**         |                 |        I left these at the defaults         |
| Custom Font Directory         | custom/myfonts  |                                             |
| Source Type                   |      Forge      |                                             |
| Default Language              |                 |                                             |
| Randomize Runes               |     Default     |                                             |
| Logographical Fonts           | &#9745;&#65039; |                                             |
| Use All Fonts                 | &#9745;&#65039; |                                             |
| Make Fonts Available          | &#9745;&#65039; |                                             |
| Journal Highloght Opacity (%) |       25        |                                             |
| Replace System Languages      |    &#128306;    |                                             |
| Custom Languages              |                 |                                             |
| Comprehend Languages Spell    |                 |                                             |
| Tongues Spell                 |                 |                                             |
| Display Translations          | &#9745;&#65039; |                                             |
| Hide Indicators From Players  | &#9745;&#65039; |                                             |
| Scramble OOC Chat Messages    |     GM Only     |                                             |
| Scramble Messages for GM      | &#9745;&#65039; |                                             |
| Replace Special Languages     |     Common      |                                             |

### [PopOut!](https://www.foundryvtt-hub.com/package/popout/) :part_alternation_mark: v2.8

This module adds a PopOut! button to most actor sheets, journal entries, and applications. The PopOut! button allows you to open a sheet/application/document into its own window, for easier viewing or for use with multiple monitors.

**NOTE:** This module does not work in the standalone FVTT Application.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/lib-wrapper/)

| Setting                         |   My Setting    | Comments |
| ------------------------------- | :-------------: | :------: |
| Show PopOut! Button             | &#9745;&#65039; |          |
| Icon Only Button                | &#9745;&#65039; |          |
| Pop Sheets out into Windows     | &#9745;&#65039; |          |
| Bounding Box Compatibility Mode |    &#128306;    |          |

### [Quick Encounters](https://www.foundryvtt-hub.com/package/quick-encounters/) v1.0.3

- Design your encounters by positioning and configuring Hostile tokens, then save the tokens into a Quick Encounter Journal Entry, marked with a single Map Note on the Scene which only you the DM see (similar to the Encounter ability in Fantasy Grounds). To run your Quick Encounter, simply double-click the Map Note icon and click the "Add to Combat Tracker" button; all your saved tokens will be positioned back on the map and added to the Combat Tracker, ready to fight!
- Quickly run encounters from converted or packaged modules by opening the relevant Journal Entry and pressing the Quick Encounter button - any embedded Actors are converted into tokens distributed around the corresponding Map Note, and added to the Combat Tracker
- Easily scale-up encounters by just increasing the number of Actors (change "3 Zombies" to "10 Zombies" in the Journal Entry) or use dice rolls (e.g. 2d6+2)
- Run Rovaming Encounter by moving the Map Note to the desired location just before activating the encounter
- Save your player tokens at the entrance to a new Scene so you don't have to drag/move them
- Shows you total XP and XP per player token after the encounter

| Setting                         |   My Setting    | Comments |
| ------------------------------- | :-------------: | :------: |
| Freeze "Captured" Tokens        | &#9745;&#65039; |          |
| Show QE Dialog Automatically    | &#9745;&#65039; |          |
| Display XP After Combat         | &#9745;&#65039; |          |
| Delete Tokens After Add/Link    | &#9745;&#65039; |          |
| Show Delete Dialog After Combat | &#9745;&#65039; |          |
| Show Add to CT                  |    &#128306;    |          |
| Check for Instant Encounters    |    &#128306;    |          |

### [Quick Insert - Search Widget](https://www.foundryvtt-hub.com/package/quick-insert/) :part_alternation_mark: v2.6.1

Quickly find characters, monsters, items, spells etc. and insert them wherever you need them right now - the map, character sheets, roll tables, journals, you name it. Just press CTRL-Space. Quick Insert provides a context-aware search/autocomplete tool that can be used in most parts of FoundryVTT. Integration with chat, character sheets (select systems), rich text editors, macro editors and the roll table editor.

| Setting                             |      My Setting       |                  Comments                   |
| ----------------------------------- | :-------------------: | :-----------------------------------------: |
| **Indexing Settings**               |                       | Holy Moly! Leave these set to the defaults. |
| **Customize Filters**               |                       |              I left this alone              |
| **Character Sheet Filters**         |                       |                                             |
| Class                               |         None          |                                             |
| Feat                                |         None          |                                             |
| NPC.Feat                            |         None          |                                             |
| Spell                               |         None          |                                             |
| Weapon                              |         None          |                                             |
| Equipment                           |         None          |                                             |
| Consumable                          |         None          |                                             |
| Backpack                            |         None          |                                             |
| Tool                                |         None          |                                             |
| Loot                                |         None          |                                             |
| **General Settings**                |                       |                                             |
| GMs Only                            |       &#128306;       |                                             |
| Character Sheet Integration         |    &#9745;&#65039;    |                                             |
| Automatic Indexing                  | Wait until First Open |                                             |
| Index Timeout                       |      1.5 seconds      |                                             |
| Search Button in Scene Controls     |    &#9745;&#65039;    |                                             |
| Enable Global Browse Mode           |    &#9745;&#65039;    |                                             |
| Default Action for Scenes           |      View Scene       |                                             |
| Default Action for Rolltable Tables |         Roll          |                                             |
| Default Action for Macros           |        Execute        |                                             |

### [QuickScale](https://www.foundryvtt-hub.com/package/quickscale/) v1.4.1

A small module for quickly adjusting the scale of things. While any token, tile, template, light, or sound is controlled, use the left and right square brackets to nudge their size. Add Shift to change the behaviour.

| Setting                         | My Setting | Comments |
| ------------------------------- | :--------: | :------: |
| Token Scale Randomization Range | 0.8 - 1.2  |          |
| Tile Scale Randomization Range  | 0.9 - 1.1  |          |
| Maximum Rotation Amount         |     15     |          |

### [Scene Preview](https://www.foundryvtt-hub.com/package/scene-preview/) v3.0.0

Shows the GM a preview image of the scene, on moving the mouse over the scene navigation scenes.

### [Sequencer](https://www.foundryvtt-hub.com/package/sequencer/) v2.1.5

This module implements a basic pipeline that can be used for managing the flow of a set of functions, effects, sounds, and macros.

It implements a fluid and very readable interface so that even common users can read exactly what the Sequencer will do.

Dependencies: [Socketlib](https://www.foundryvtt-hub.com/package/socketlib/)

| Setting                          |       My Setting        | Comments |
| -------------------------------- | :---------------------: | :------: |
| Enable Debugging                 |        &#128306;        |          |
| Show Sequencer Tools             |     &#9745;&#65039;     |          |
| Enable Effects                   |     &#9745;&#65039;     |          |
| Enable Sounds                    |     &#9745;&#65039;     |          |
| External Effect Opacity          |           50            |          |
| Sequencer: Play Effects          |    Players and Above    |          |
| Sequencer: Remove All Effects    | Assistant GMs and Above |          |
| Sequencer: Play Sounds           |    Plyers and Above     |          |
| Sequencer: Preloading for Others | Assistant GMs and Above |          |
| Sequencer: Use Sidebar Tools     |    Players and Above    |          |

### [Settings Extender](https://www.foundryvtt-hub.com/package/settings-extender/) v1.2.0

Extends the settings menu, adding additional types for configuration values.

### [Show Door Controls](https://www.foundryvtt-hub.com/package/showdooricons/) v1.0.1

Forces Door Controls to remain visible on wall layer for easier visibilty.

| Setting                    |   My Setting    | Comments |
| -------------------------- | :-------------: | :------: |
| Force Door Icon Visibility | &#9745;&#65039; |          |

### [Simple Calendar](https://www.foundryvtt-hub.com/package/foundryvtt-simple-calendar/) :part_alternation_mark: v2.0.25

This module allows you to create a calendar with any number of months per year, any number of days per month and customizable hours, minutes and seconds for your game world. It is intended as a way for a GM to show a calendar like interface that maps to their in game world.

| Setting                                 |                               My Setting                                |                Comments                 |
| --------------------------------------- | :---------------------------------------------------------------------: | :-------------------------------------: |
| **Global Configuration**                |                                                                         |                                         |
| _Settings_                              |                                                                         |                                         |
| Sync Date/Time Changes Across Calendars |                             &#9745;&#65039;                             |                                         |
| Seconds Per Combat Round                |                                    6                                    |                                         |
| Show Notes Folder                       |                                &#128306;                                |                                         |
| _Client Settings_                       |                                                                         |                                         |
| Theme                                   |                                  Dark                                   |                                         |
| Open On Load                            |                                &#128306;                                |                                         |
| Open In Compact View                    |                                &#128306;                                |                                         |
| Remember Position                       |                             &#9745;&#65039;                             |                                         |
| _Permissions_                           |                                                                         |                                         |
| View Calendar                           |     &#9745;&#65039; &#9745;&#65039; &#9745;&#65039; &#9745;&#65039;     |                                         |
| Add Notes                               |              &#128306; &#128306; &#128306; &#9745;&#65039;              |                                         |
| Reorder Notes                           |              &#128306; &#128306; &#128306; &#9745;&#65039;              |                                         |
| Change Date and Time                    |              &#128306; &#128306; &#128306; &#9745;&#65039;              |                                         |
| Change Active Calendar                  |              &#128306; &#128306; &#128306; &#9745;&#65039;              |                                         |
| **Calendar Default**                    |                                                                         |                                         |
| _Quick Setup_                           |                        Forgotten Realms: Harptos                        | Use this to save yourself a LOT of work |
| **Settings**                            |                                                                         |                                         |
| _General Settings_                      |                                                                         |                                         |
| Calendar Name                           |                                 Harptos                                 |                                         |
| Game World Time Integration             | Mixed: Update the game world time and follow updates from other modules |                                         |
| _Display Options_                       |                                                                         |                                         |
| Date Format                             |                              MMMM DD, YYYY                              |                                         |
| Time Format                             |                                HH:mm:ss                                 |                                         |
| Month/Year Format                       |                              MMMM YAYYYYYZ                              |                                         |
| _Year Settings_                         |                                                                         |        Configured by Quick Setup        |
| _Month Settings_                        |                                                                         |        Configured by Quick Setup        |
| _Weekday Settings_                      |                                                                         |        Configured by Quick Setup        |
| _Leap Year Settings_                    |                                                                         |        Configured by Quick Setup        |
| _Season Settings_                       |                                                                         |        Configured by Quick Setup        |
| _Moon Settings_                         |                                                                         |        Configured by Quick Setup        |
| _Time Settings_                         |                                                                         |        Configured by Quick Setup        |
| _Note Settings_                         |                                                                         |        Configured by Quick Setup        |

### [SmallTime](https://www.foundryvtt-hub.com/package/smalltime/) :part_alternation_mark: v1.14.1

A small module for displaying and controlling the current time of day.

| Setting                           |            My Setting             | Comments |
| --------------------------------- | :-------------------------------: | :------: |
| Player Visibility Default         | Sun/Moon Position and Time of Day |          |
| Time Format                       |               12hr                |          |
| Date Format                       |        Hammer 1st, 1481 DR        |          |
| Calendar Provider                 |          Simple Calendar          |          |
| Small Step Amount                 |                10                 |          |
| Large Step Amount                 |                60                 |          |
| Resting Opacity                   |                0.8                |          |
| Synchronize Sunrise/Sunset        |          &#9745;&#65039;          |          |
| Affect Darkness by Default        |          &#9745;&#65039;          |          |
| Moon Phase Affects Darkness Level |          &#9745;&#65039;          |          |
| Allow Trusted Player Control      |             &#128306;             |          |

### [Small Visual Tweaks](https://www.foundryvtt-hub.com/package/svt/) v1.1.1

Formerly known as 'Playlist Down', this module adds some small visual tweaks to Foundry's UI. It currently highlights the 'Currently Playing' tracks, truncates long names in chat, and adds badges to more easily identify Private/Blind Rolls and Whispers.

### [Smart Doors](https://www.foundryvtt-hub.com/package/smart-doors/) v1.3.3

Consistent Door Control Size
Door Control icons will be rendered the same size in every scene, regardless of the configured grid size. The size of the icons is configurable.

Toggle Secret Doors
Easily reveal secret doors to players. Ctrl+left click secrets doors to turn them into regular doors. Ctrl+left click can also be done on normal doors to turn them into secret doors.

Toggle Secret Doors demonstration

Locked Door Alerts
Keep everyone informed who tried to open which door. If a player tries to open a locked door the locked door sound will be played for everyone. Additionally a message will be posted to chat. When the chat message is hovered with the mouse, the door that was attempted to open will be highlighted.

Door Synchronization
Doors can be added to synchronization groups. Whenever a door belonging to a synchronization group is opened, closed, locked or unlocked the same will automatically happen to all other doors in the synchronization group. Usage instructions can be found in the modules README.

| Setting                  |   My Setting    | Comments |
| ------------------------ | :-------------: | :------: |
| Door Control Size Factor |       1.5       |          |
| Tint Secret Doors        | &#9745;&#65039; |          |
| Locked Door Alert        | &#9745;&#65039; |          |
| Synchronized Doors       | &#9745;&#65039; |          |

### [Smart Target](https://www.foundryvtt-hub.com/package/smarttarget/) :part_alternation_mark: v0.8.5

A module to target with Alt+Click and show portraits instead of colored pips to indicate targets, positioning offset and size of the icons can be configured in the module settings.

Dependencies: [LibWrapper](https://www.foundryvtt-hub.com/package/libwrapper/)

| Setting                                    |   My Setting    | Comments |
| ------------------------------------------ | :-------------: | :------: |
| Targeting Mode                             |    Alt-Click    |          |
| Release Behavior                           |    Standard     |          |
| Show Indicator Portraits Instead of Colors | &#9745;&#65039; |          |
| Use Tokens Instead of Avatars              |    &#128306;    |          |
| GM Image                                   |  Player Avatar  |          |
| Target Indicator Position                  |    Top Left     |          |
| Keep Target indicators Inside the Token    |    &#128306;    |          |
| Target Icon image Scale                    |        1        |          |
| Target Image Y Offset                      |        0        |          |
| Target image X Offset                      |        0        |          |
| Target Icon Size                           |       12        |          |
| Target Icon Offset                         |       16        |          |
| Border Thickness                           |        2        |          |
| Bring Targetting Arrows Closer Together    | &#9745;&#65039; |          |
| Targeting Arrows Color                     |    #0091ff80    |          |
| Target Indicator                           |  Better Target  |          |
| Use Player Color for Target Indicator      | &#9745;&#65039; |          |

### [SocketLib](https://www.foundryvtt-hub.com/package/socketlib/) v1.0.10

This library makes it easy to execute functions in the clients of other connected users.

### [Splatter](https://www.foundryvtt-hub.com/package/splatter/) v2.9.7

Add blood and gore to your games.

- Bood on hit. The bigger the hit, the more the blood. Configure manual blood scale, violence level, and blood trails
- Blood by creature type. Set up a configuration for blood color and creature type, by default one for dnd5e is included
- Per Token Blood color. Set the blood color differently for each token

| Setting                        |        My Setting        | Comments |
| ------------------------------ | :----------------------: | :------: |
| Blood Color                    |        #a51414d8         |          |
| Enable Bloodsplats             |     &#9745;&#65039;      |          |
| Violence Level                 |            1             |          |
| Cleanup                        |            5             |          |
| Walls Block Blood              |     &#9745;&#65039;      |          |
| Blood Trail                    |     &#9745;&#65039;      |          |
| Automatic Blood Color          |     &#9745;&#65039;      |          |
| Bloodsplat Threshold           |            50            |          |
| Bloodsplats Scale              |           0.5            |          |
| Delay                          |           500            |          |
| Only in Combat                 |     &#9745;&#65039;      |          |
| Sync With Automated Animations |     &#9745;&#65039;      |          |
| Creature Type                  | data.details.type.value  |          |
| Custom Creature Type           | data.details.type.custom |          |
| Current HP                     | data.attributes.hp.value |          |
| Max HP                         |  data.attributes.hp.max  |          |
| Wounds System                  |        &#128306;         |          |

### [Status Icon Counters](https://www.foundryvtt-hub.com/package/statuscounter/) v2.0.11

Status Icon Counters allows setting and displaying a counter on any token's status effects.

| Setting              |   My Setting    | Comments |
| -------------------- | :-------------: | :------: |
| Rebind Mouse Buttons | &#9745;&#65039; |          |
| Rebind Number Keys   | &#9745;&#65039; |          |
| Display '1'          |      Never      |          |
| Counter Font SIze    |       16        |          |
| Counter Color        |     00ffff      |          |
| COuntdown Color      |     ffff00      |          |

### [Table Ninja](https://www.foundryvtt-hub.com/package/table-ninja/) v0.1.1

Quicklv derive a whole host of random data from various tables for your perusal.

### [Tagger](https://www.foundryvtt-hub.com/package/tagger/) v1.3.4

This module allows you to tag PlaceableObjects in the scene and retrieve them just as easy. All major PlaceableObjects' configuration dialogues (such as actor prototype tokens, tokens, tiles, walls, lights, etc), now has a "Tags" field. Each tag is separated by a comma.

### [Talis Dice](https://www.foundryvtt-hub.com/package/talisdice/) v1.0.6

Texture sets for Dice So Nice!

Dependencies: [Dice So Nice](https://www.foundryvtt-hub.com/package/dice-so-nice/)

### [Terrain Ruler](https://www.foundryvtt-hub.com/package/terrain-ruler/) v1.6.0

This module makes Foundry's rulers respect difficult terrain that was put down using the Enhanced Terrain Layer module. In addition, other modules can use it as a library to easily perform measurements that take into account difficult terrain.

| Setting              | My Setting | Comments |
| -------------------- | :--------: | :------: |
| Default Toggle State |     On     |          |

### The Forge v

| Setting                                   |   My Setting    | Comments |
| ----------------------------------------- | :-------------: | :------: |
| API Secret Key                            |                 |          |
| Use Cloudflare Connections (Experimental) | &#9745;&#65039; |          |

### [The Forge: Compendium Library](https://www.foundryvtt-hub.com/package/the-forge-compendium-library/) v1.1.0

Browse your compendium books like they were meant to be viewed. The Forge's Compendium Library lets you navigate vour D&D Bevond imported books in a beautiful and easv to use interface!

Dependencies: [dlopen](https://www.foundryvtt-hub.com/package/dlopen/), [VuePort: Let the Lght In!](https://www.foundryvtt-hub.com/package/vueport/)

| Setting                  |   My Setting    | Comments |
| ------------------------ | :-------------: | :------: |
| Auto Open With Same Name | &#9745;&#65039; |          |

### The Forge: More Awesomeness v1.1

This is an optional module provided by The Forge to fix various issues and bring its own improvements to Foundry VTT.

### [Tidy5e Sheet](https://www.foundryvtt-hub.com/package/tidy5e-sheet/) :part_alternation_mark: v0.5.32

Tidy5e Sheet is an alternate Actor Sheet for the dnd5e Game System aimed at creating a cleaner user interface. It replaces the default Character Sheet, NPC and vehicles sheet and alters the item sheet. Several Functions have been moved to be more visible and easier to use and the Layout has been adjusted to better fit the contents and make the overall layout more scanable. The item sheets were treated in the same way to better fit the overall appearance.

| Setting                                             |                       My Setting                        | Comments |
| --------------------------------------------------- | :-----------------------------------------------------: | :------: | --- |
| **Sheet Settings**                                  |                                                         |          |
| _Players_                                           |                                                         |          |
| Hide Character Journal                              |                        &#128306;                        |          |
| Hide Character Class List                           |                        &#128306;                        |          |
| Inanimate Inspiration Tracker                       |                        &#128306;                        |          |
| Hide Exhaustion and Inspiration Tracker on 0        |                     &#9745;&#65039;                     |          |
| Show Inspiration Indicator Only on Hover            |                        &#128306;                        |          |
| Show Exhaustion Tracker Only on Hover               |                        &#128306;                        |          |
| Disable Health Bar                                  |                        &#128306;                        |          |
| Hide Hit Point Overlay                              |                        &#128306;                        |          |
| Toggle Empty Character Traits                       |                        &#128306;                        |          |
| Move Traits Below Resources                         |                        &#128306;                        |          |
| Only Show Equipped Ammunition                       |                        &#128306;                        |          |
| Default Player Sheet Width                          |                           740                           |          |
| _NPCs/Vehicles_                                     |                                                         |          |
| - NPCs                                              |                                                         |          |
| Resting for NPCs                                    |                        &#128306;                        |          |
| Hide Chat Card for NPC Rest                         |                     &#9745;&#65039;                     |          |
| Mark Linked/Unlinked NPCs                           |                No Link Marker (default)                 |          |
| Disable Health Bar                                  |                        &#128306;                        |          |
| Hide Hit Point Overlay                              |                        &#128306;                        |          |
| Always Show Traits                                  |                        &#128306;                        |          |
| Move Traits Below Resources                         |                        &#128306;                        |          |
| Always Show Skills                                  |                        &#128306;                        |          |
| Default NPC Sheet Width                             |                           740                           |          |
| - Vehicles                                          |                                                         |          |
| Disable Health Bar                                  |                        &#128306;                        |          |
| Hide Hit Point Overlay                              |                        &#128306;                        |          |
| Default Vehicle Sheet Width                         |                           740                           |          |
| _GM Options_                                        |                                                         |          |
| Hide Death Saves From Players                       |                        &#128306;                        |          |
| Show Player Name                                    |                     &#9745;&#65039;                     |          |
| Disable Right-Click Context Menu                    |                        &#128306;                        |          |
| Expanded Limited View For Character Sheets          |                        &#128306;                        |          |
| Key to Hold for Item Card Interaction               |                            X                            |          |
| Round Portrait Style                                |                       All Actors                        |          |
| - Offset Hit Point Overlay                          |                                                         |          |
| PCs                                                 |                            0                            |          |
| NPCs                                                |                            0                            |          |
| Vehicles                                            |                            0                            |          |
| Lock Down Sensitive Fields                          |                        &#128306;                        |          |
| GM Can Always Edit PC Sheets                        |                     &#9745;&#65039;                     |          |
| Only GM Can Edit Active Effects                     |                     &#9745;&#65039;                     |          |
| Always Show Item Quantity                           |                        &#128306;                        |          |
| Disable Inspiration Tracker                         |                        &#128306;                        |          |
| Auto Exhaustion Effects                             | Tidy5e - Apply Integrated Effects (uses Active Effects) |          |
| Custom Exhaustion Effect Icon                       |       modules/tidy5e-sheet/images/exhaustion.svg        |          |
| Disable Exhaustion Tracker                          |                        &#128306;                        |          |
| Show Trait Labels                                   |                     &#9745;&#65039;                     |          |
| _Modules_                                           |                                                         |          |
| - Actor Actions / Favorites                         |                                                         |          |
| Default Tab When Opening a Sheet For the First Time |                         Actions                         |          |
| - MidiQOL                                           |                                                         |          |
| Show MidiAOL Item Buttons in Item's Info Card       |                     &#9745;&#65039                      |          |
| Show Active Effects Marker on Items                 |                     &#9745;&#65039                      |          |
| **General Settings**                                |                                                         |          |
| Tidy5e Theme                                        |                    Alternate (dark)                     |          |     |
| Use Classic Item Controls in List View              |                        &#128306;                        |          |
| Show Item Info Cards in All Layouts                 |                     &#9745;&#65039                      |          |
| Show Item Info Cards for NPCs/Vehicles              |                     &#9745;&#65039                      |          |
| Item Info Cards Float Next to Cursor                |                        &#128306;                        |          |
| \ Delay Showing Info Cards                          |                           300                           |          |

### [Times Up](https://www.foundryvtt-hub.com/package/times-up/) v0.8.32

This module is a temporary solution to Active Effects being removed from actors as time passes and they expire.

- Times Up automatically expire Active Effects as the duration reaches 0.
- Times Up supports time (seconds) based and round/turn based expriy of effects.

| Setting                              | My Setting | Comments |
| ------------------------------------ | :--------: | :------: |
| Disable Passive Effects After Expiry | &#128306;  |          |
| Debug                                |    None    |          |

### [Token Action HUD](https://www.foundryvtt-hub.com/package/token-action-hud/) :part_alternation_mark: v2.2.17

This module populates a floating HUD, showing common actions for a controllable token. It shows items, feats, ability checks, and spells. The intent is to save GMs and players from having to frequently open character sheets to access actions, and to allow people to focus on the table.

| Setting                                             |           My Setting            | Comments |
| --------------------------------------------------- | :-----------------------------: | :------: |
| HUD Roll Handler                                    | Core D&D 5e (Supports Midi-QOL) |          |
| Enable Doraco UI                                    |         &#9745;&#65039;         |          |
| Enable HUD for Current User                         |         &#9745;&#65039;         |          |
| Always Display HUD                                  |         &#9745;&#65039;         |          |
| Show HUD Title                                      |         &#9745;&#65039;         |          |
| Show Action Icons                                   |         &#9745;&#65039;         |          |
| Always Show Custom Categories                       |            &#128306;            |          |
| Enable Hovering                                     |            &#128306;            |          |
| Click-to-Open Categories                            |            &#128306;            |          |
| Ignore Passive Feats                                |            &#128306;            |          |
| Display Spell Information                           |         &#9745;&#65039;         |          |
| Show All Non-Preparable Spells                      |         &#9745;&#65039;         |          |
| Hide Actions With an Activation Longer Than 1 Round |            &#128306;            |          |
| Abbreviate Skill and Ability Names                  |            &#128306;            |          |
| Show Separate Ability Check and Save Categories     |         &#9745;&#65039;         |          |
| Show All NPC Items                                  |         &#9745;&#65039;         |          |
| Show Empty Items (Includes Items and Spells)        |            &#128306;            |          |
| Show Conditions Category                            |         &#9745;&#65039;         |          |
| Show Items Without Activation Costs                 |            &#128306;            |          |
| Item-Macro: item macro, original item, or both      |    Show The Item Macro Item     |          |
| Enable HUD for Players                              |         &#9745;&#65039;         |          |
| Show Item Sheet on Right-Click                      |         &#9745;&#65039;         |          |
| HUD Scale                                           |                1                |          |
| HUD Background Color                                |            #00000040            |          |
| Mark Active CSS With Text                           |            &#128306;            |          |
| Dropdown Categories                                 |         &#9745;&#65039;         |          |
| Enable Debugging                                    |            &#128306;            |          |

### [Token Attacher](https://www.foundryvtt-hub.com/package/token-attacher/) v4.4.5

Attach anything (even other tokens and their attached elements as well) to tokens, so that they move when the token moves and rotate/move when the token rotates. Resizing the base token will also resize all attached elements.

| Setting                                    | My Setting | Comments |
| ------------------------------------------ | :--------: | :------: |
| Prevent Movement on MLT/V&M on Token Bases | &#128306;  |          |

### [Token Magic FX](https://www.foundryvtt-hub.com/package/tokenmagic/) :part_alternation_mark: v0.5.4

Add visual effects to templates, drawings, tokens and tiles. The special effects can be animated. Token Magic FX comes with a compendium of macros for each effect, easily modifiable to suit your needs. A graphical interface is provided for templates. A graphical interface will be added in later version for other placeables.

| Setting                              |   My Setting    |      Comments       |
| ------------------------------------ | :-------------: | :-----------------: |
| **Automatic Template Effects**       |                 | Accept all defaults |
| **General Settings**                 |                 |                     |
| Enable Automatic Template Effects    | &#9745;&#65039; |                     |
| Default Template Grid on Hover       | &#9745;&#65039; |                     |
| Automaitcally Hide Template Elements | &#9745;&#65039; |                     |
| FX in Additive Padding Mode          |    &#128306;    |                     |
| Minimum Padding                      |       50        |                     |
| Permissive Mode                      |    &#128306;    |                     |
| Overwrite on Import                  |    &#128306;    |                     |
| Order the FX                         |    &#128306;    |                     |
| Disable FX Animations                |    &#128306;    |                     |
| Disable Filter Caching on Startup    | &#9745;&#65039; |                     |
| Disable Video Support in Templates   |    &#128306;    |                     |

### [Token Mold](https://www.foundryvtt-hub.com/package/token-mold/) v2.15.1

Adds options to customize the token creation workflow, as well as auras and a quick attribute checker when hovering over a token.

Feature Overview

- Automatic token indexing. (Appending an increasing number to tokens of a same kind on creation)
- Random Name Generation from 35(!) languages
- Override token config
- Customizable overlay on token hover, for quick reference
- Automatic name hiding
- Hit Point rolling by formula (currently dnd5e only)
- Automatic token scaling according to creature size and map grid distance. (dnd5e and pf2e only)

| Setting                                                                         |                     My Setting                      |                    Comments                     |
| ------------------------------------------------------------------------------- | :-------------------------------------------------: | :---------------------------------------------: |
| Apply Settings to unlinked Actors ONLY                                          |                      &#128306;                      |                                                 |
| **Name**                                                                        |                                                     |                                                 |
| Add Counting Numbers to name as Suffix                                          |                      &#128306;                      |                                                 |
| Choose Number Style                                                             |              " (" arabic numerals ")"               |                                                 |
| Increase Index by Up To                                                         |                          1                          |                                                 |
| Add Random Adjective From Dictionary                                            |                      &#128306;                      |                                                 |
| Choose Adjectives From the Following Rollable Table                             | English - Smaller but Community Curated List of 700 |                                                 |
| Adjective Placement                                                             |                        Front                        |                                                 |
| Base Name                                                                       |                     Do Nothing                      |                                                 |
| Hold SHIFT To Override Name Remvoal or Replacement And Always Add the Base Name |                      &#128306;                      |                                                 |
| **System Specific**                                                             |                                                     |                                                 |
| Send Result to Chat                                                             |                      &#128306;                      |                                                 |
| Set Token Size to Creature Size and Scaled ot Map Scale                         |                   &#9745;&#65039;                   |                                                 |
| **Modify Default Config**                                                       |                                                     | Change these as you see fit. I left them alone. |
| Display Bars                                                                    |                   &#128306; OWNER                   |                                                 |
| Bar 1 Attribute                                                                 |                    &#128306;None                    |                                                 |
| Bar 2 Attribute                                                                 |                   &#128306; None                    |                                                 |
| Display Name                                                                    |                   &#128306; OWNER                   |                                                 |
| Token Disposition                                                               |                  &#128306; NEUTRAL                  |                                                 |
| Vision                                                                          |        &#128306; &#9745;&#65039; Has Vision         |                                                 |
| Random Scale Multiplier                                                         |            &#128306; Min: 0.8, Max: 1.2             |                                                 |
| Random Rotation Range                                                           |       &#128306; Min Angle: 0, Max Angle: 360        |                                                 |
| Random Mirroring                                                                |                 &#128306; &#128306;                 |                                                 |
| **Stat Overlay**                                                                |                                                     |                                                 |
| 1st Attribute                                                                   | Icon: Shield, Attribute: ac.value [character, npc]  |                                                 |
| 2nd Attribute                                                                   | Icon: Eye, Attribute: prc.passive [character, npc]  |                                                 |

### [Token Variant Art](https://www.foundryvtt-hub.com/package/token-variants/) v2.4.0

Searches a customisable list of directories and displays art variants for tokens/actors through pop-ups and a new Token HUD button. Variants can be individually shared with players allowing them to switch out their token art on the fly.

| Setting | My Setting | Comments |
| ------- | :--------: | :------: |

### [Universal Battlemap Importer](https://www.foundryvtt-hub.com/package/dd-import/) :part_alternation_mark: v2.3.0

Allows Importing Dungeondraft, DungeonFog or Arkenforge export files into FoundryVTT.

| Setting          |   My Setting    | Comments |
| ---------------- | :-------------: | :------: |
| Openable Windows | &#9745;&#65039; |          |

### [VuePort: Let the Light in!](https://www.foundryvtt-hub.com/package/vueport/) v1.1

Using Handlebars is like switching from Theatre of the Mind to using a ruler to help tracing your grid in your pen & paper game. Using Vue is like switching to Foundry VTT instead! Open the VuePort, let the light in and bask in the glory! This library module, contributed by The Forge, provides an easy way to integrate Vue.js within your Foundry applications. See README for details on how to use it.

| Setting          | My Setting | Comments |
| ---------------- | :--------: | :------: |
| Enable Vue Debug | &#128306;  |          |

### [Wall Height](https://www.foundryvtt-hub.com/package/wall-height/) :part_alternation_mark: v4.3

Adds the ability to set wall height for walls so that tokens can look over them (or under them).

| Setting                                    |   My Setting    | Comments |
| ------------------------------------------ | :-------------: | :------: |
| Enable Tooltip                             |    &#128306;    |          |
| Display Height on Walls                    | &#9745;&#65039; |          |
| Vaulting                                   | &#9745;&#65039; |          |
| Automatic Token Height                     | &#9745;&#65039; |          |
| Default Token Height                       |        6        |          |
| Enable "Constrained By Elevation" Globally | &#9745;&#65039; |          |
| Migrate Wall Height Data on Startup        |    &#128306;    |          |

### [Warp Gate](https://www.foundryvtt-hub.com/package/warpgate/) v1.13.6

Warp Gate is a system-agnostic library module that provides a few public API functions to make programmatically spawning tokens and modifying those tokens easier for players and GMs alike.

| Setting                         |      My Setting      | Comments |
| ------------------------------- | :------------------: | :------: |
| Debug                           |      &#128306;       |          |
| Unrestricted Dismissal          |      &#128306;       |          |
| Update Delay (ms)               |          20          |          |
| Always Accpet Mutation Requests |      &#128306;       |          |
| Show Dismiss Button Label       |   &#9745;&#65039;    |          |
| Show Revert Button Label        |   &#9745;&#65039;    |          |
| Dismissed Button Scope          | Spawned Tokens Only  |          |
| Revert Button Behavior          | Revert last Mutation |          |

### [Weather Blocker](https://www.foundryvtt-hub.com/package/weatherblock/) v1.0.2

Just create a drawing (Only Polygons are supported), and in the text field type `blockWeather`

### [WebRTC Tweaks](https://www.foundryvtt-hub.com/package/webrtc-tweaks/) v0.6.1

Add position button to move all of video windows to different sides of the screen (left vertical, top horizonal, right vertical, and original bottom horizontal)

| Setting              | My Setting | Comments |
| -------------------- | :--------: | :------: |
| Enable Debug Logging | &#128306;  |          |

### [Window Controls](https://www.foundryvtt-hub.com/package/window-controls/) v1.9.8

Window Taskbar and Window Buttons: Minimize, Maximize and Pin floating Windows. Configurable.

Organized Minimized mode allows minimizing windows on a fixed horizontal taskbar located on top or bottom.

| Setting                                         |   My Setting    | Comments |
| ----------------------------------------------- | :-------------: | :------: |
| Organized Minimize                              |     Bottom      |          |
| Minimize Button                                 |     Enabled     |          |
| Pinned Button                                   |     Enabled     |          |
| Maximize Button                                 |    Disabled     |          |
| Minimize Everything on Outside Click            |    &#128306;    |          |
| Double "ESC" Tapping to Minimize Pinned Windows | &#9745;&#65039; |          |
| Remember Pinned Windows                         | &#9745;&#65039; |          |
| Taskbar Color                                   |     #800000     |          |

### [Wonderwalls](https://www.foundryvtt-hub.com/package/wonderwalls/) v0.1.03

Adds a Walking and Eye icon to represent different wall data types. A Walking icon will be added to a wall if it does not restrict movement. An Eye icon will be added to a wall if it does not restrict vision

### [Zoom/Pan Options](https://www.foundryvtt-hub.com/package/zoom-pan-options/) v1.7.0

Change zooming and panning through the mouse or a touchpad. It has several settings, which can be toggled individually and stored locally (per client).

| Setting                       |            My Setting            | Comments |
| ----------------------------- | :------------------------------: | :------: |
| Zoom Around Cursor            |         &#9745;&#65039;          |          |
| Middle Mouse to Pan           |         &#9745;&#65039;          |          |
| Disable Zoom Rounding         |         &#9745;&#65039;          |          |
| Minimum/Maximum Zoom Override |                3                 |          |
| Pan/Zoom Mode                 | Mouse: Standard Foundry Behavior |          |
| Zoom Speed                    |                0                 |          |
| Pan Speed                     |                1                 |          |
| Invert Vertical Scroll        |            &#128306;             |          |
| "pad" Value When Dragging     |                50                |          |
| "Shift" Value When Dragging   |                3                 |          |
